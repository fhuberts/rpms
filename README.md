# RPMs

This repository contain a collection of useful RPMs.

The RPMs are mainly for Fedora, but can be adjust for RHEL/CentOS by willing
users: pull requests are welcome.


# Layout

The RPMs are split by which repositories they need.

For example: if package X can be built against only the Fedora repositories
then it will be located in the ```fedora``` directory. If that package needs the
RPMFusion-Free repositories then it will be located in the ```rpmfusion_free```
directory.

The directory names correlate to the mock configuration files
in ```/etc/mock```.


# Building

Building the RPMs in  this repository requires a parallel checkout of the
```rpmbuilder``` repository, located
[here on GitLab](https://gitlab.com/fhuberts/rpmbuilder).

The [RPMFusion](http://rpmfusion.org/) repository must be enabled on the system
in order to be able to build all RPMs in this repository.

At least the following packages must be installed before building:

```
rpmdevtools.noarch
mock.noarch
mock-rpmfusion-free.noarch
```

All tools needed for building the RPMs can be installed by running

```
make prereq
```

This does NOT install repository specific packages like
```mock-rpmfusion-free.noarch```.


## All RPMs

**All** RPMs are built for the current system by running

```
make
```

To build with mock for Fedora 27 x86_64 run

```
make VERBOSE=1 DIST=fedora-27-x86_64-rpmfusion_free
```


## Fedora RPMs

All **Fedora** RPMs are built for the current system by running

```
make -C fedora
```

To build with mock for Fedora 27 x86_64 run

```
make -C fedora VERBOSE=1 DIST=fedora-27-x86_64
```


## RPMFusion Free RPMs

All **RPMFusion-Free** RPMs are built for the current system by running

```
make -C rpmfusion_free
```

To build with mock for Fedora 27 x86_64 run

```
make -C rpmfusion_free VERBOSE=1 DIST=fedora-27-x86_64-rpmfusion_free
```
