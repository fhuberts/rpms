%global vsftpConfigFile     %{_sysconfdir}/vsftpd/vsftpd.conf
%global vsftpChrootFile     %{_sysconfdir}/vsftpd/chroot_list
%global avahiFtpServiceFile %{_sysconfdir}/avahi/services/ftp.service
%global pasvPortMin         4242
%global pasvPortMax         5241

Name:             ftp-server
Version:          1.2.1
Release:          3%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          A VSFTPd based FTP server with (ch)rooted users

Requires:         configfile-utils

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils
Requires:         firewalld
Requires:         glibc-common, grep
Requires:         passwd, policycoreutils-python-utils
Requires:         sed, shadow-utils, sudo
Requires:         systemd
Requires:         vsftpd


%package avahi
Summary:          Advertise FTP service via Avahi

Requires:         ftp-server

Requires:         avahi, avahi-dnsconfd
Requires:         hostname
Requires:         sed


%description
A VSFTPd based FTP server with (ch)rooted users


%description avahi
Advertise FTP service via Avahi


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

configfile-adjust-settings \
  -b -f "%{vsftpConfigFile}" \
  "allow_writeable_chroot=YES" \
  "anonymous_enable=NO" \
  "chroot_list_enable=YES" \
  "chroot_list_file=%{vsftpChrootFile}" \
  "chroot_local_user=YES" \
  "listen=YES" \
  "listen_ipv6=NO" \
  "local_root=ftproot" \
  "pasv_enable=YES" \
  "use_localtime=YES"

configfile-adjust-settings \
  -n -b -f "%{vsftpConfigFile}" \
  "pasv_max_port=%{pasvPortMax}" \
  "pasv_min_port=%{pasvPortMin}"

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "vsftpd.service"
fi

if [[ $1 -eq 1 ]]; then
  # install

  setsebool -P \
    ftpd_full_access=true \
    ftpd_use_passive_mode=true

  for i in "" "--permanent"; do
    firewall-cmd -q $i --add-service=ftp
  done
  ftp-server-passive-ports -a

  systemctl -q enable            "vsftpd.service"
  systemctl -q reload-or-restart "vsftpd.service"
fi

exit 0


%post avahi

# install and updates

sed -r -i "s/(<name>).*(<\/name>)/\1$(hostname) (FTP)\2/" "%{avahiFtpServiceFile}"

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "avahi-dnsconfd.service"
fi

if [[ $1 -eq 1 ]]; then
  # install

  systemctl -q enable            "avahi-dnsconfd.service"
  systemctl -q reload-or-restart "avahi-dnsconfd.service"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  setsebool -P \
    ftpd_full_access=false \
    ftpd_use_passive_mode=false

  for i in "" "--permanent"; do
    firewall-cmd -q $i --remove-service=ftp
  done
  ftp-server-passive-ports -r

  systemctl -q stop    "vsftpd.service"
  systemctl -q disable "vsftpd.service"
fi

exit 0


%preun avahi

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart "avahi-dnsconfd.service"
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/vsftpd/*
%{_bindir}/*
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%files avahi
%config(noreplace) %{_sysconfdir}/avahi/services/*


%changelog
* Sun Apr 18 2021 Ferry Huberts - 1.2.1
- Improve the scripts
- Improve the spec file
- Tweak starting services

* Wed Feb 24 2021 Ferry Huberts - 1.2.0
- Improve usage and add bash completion

* Sun Jul 05 2020 Ferry Huberts - 1.1.23
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.1.22
- The repository moved to GitLab

* Sat May 05 2018 Ferry Huberts - 1.1.21
- Fedora 28 does a shell check for ftp

* Sun Dec 31 2017 Ferry Huberts - 1.1.20
- Bump to Fedora 27

* Mon Apr 17 2017 Ferry Huberts - 1.1.19
- Use bash' [[ builtin instead of the program [

* Thu Mar 02 2017 Ferry Huberts - 1.1.18
- Improve %post scriptlets
- Remove %preun scriptlet

* Wed Dec 28 2016 Ferry Huberts - 1.1.17
- systemctl changes

* Wed Dec 14 2016 Ferry Huberts - 1.1.16
- Update 'Requires'

* Tue Dec 06 2016 Ferry Huberts - 1.1.15
- Bump to Fedora 25

* Sat May 21 2016 Ferry Huberts - 1.1.14
- Adjust to recent SELinux policy changes

* Wed Dec 23 2015 Ferry Huberts - 1.1.13
- make sure to not overwrite passive ports on install/update
- only adjust SELinux booleans when needed
- mark files under /etc as config
- move ftp-server-info to /usr/bin

* Mon Dec 21 2015 Ferry Huberts - 1.1.12
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.1.11
- only modify services when needed

* Wed Dec 16 2015 Ferry Huberts - 1.1.10
- add the avahi sub-package

* Tue Dec 15 2015 Ferry Huberts - 1.1.9
- use the new helper script of package configfile-utils

* Tue Dec 15 2015 Ferry Huberts - 1.1.8
- ensure the optionIndex variable is an integer;
  otherwise incrementing it doesn't do the expected thing

* Mon Dec 14 2015 Ferry Huberts - 1.1.7
- add forgotten dependency on systemd

* Sun Dec 13 2015 Ferry Huberts - 1.1.6
- improve changing vsftpd settings

* Sat Dec 12 2015 Ferry Huberts - 1.1.4
- ftp-server-passive-ports: add a remark that the vsftp daemon is not restart on modify

* Sat Dec 12 2015 Ferry Huberts - 1.1.3
- cleanup and simplify scripting
- ftp-server-add-user no longer tries to set the password when it is empty
- ftp-server-info no longer tries to resolve the users root

* Thu Dec 10 2015 Ferry Huberts - 1.1.2
- force setting the password

* Thu Dec 10 2015 Ferry Huberts - 1.1.1
- do not error out when a user already exists

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- add 'print' mode to ftp-server-passive-ports
- refresh and clean up 'Requires'
- only support Fedora 23 and higher

* Wed Dec 09 2015 Ferry Huberts - 1.0.0
- Initial release
