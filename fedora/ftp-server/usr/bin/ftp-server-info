#!/usr/bin/bash

set -e
set -u

script="$0"

source "/usr/share/ftp-server/functions.include"




################################################################################
#
#
# Functions
#
#
################################################################################

function printPassivePorts() {
  local regexMin='[[:space:]]*pasv_min_port[[:space:]]*=[[:space:]]*(.+)[[:space:]]*'
  local regexMax='[[:space:]]*pasv_max_port[[:space:]]*=[[:space:]]*(.+)[[:space:]]*'
  local portMin="$(grep -E "^$regexMin\$" "$VSFTPCONFIGFILE" 2> /dev/null | \
                   sed -r "s/^$regexMin\$/\1/" 2> /dev/null)"
  local portMax="$(grep -E "^$regexMax\$" "$VSFTPCONFIGFILE" 2> /dev/null | \
                   sed -r "s/^$regexMax\$/\1/" 2> /dev/null)"
  echo "$portMin $portMax"
}


function printFtpRootDir() {
  local regex='[[:space:]]*local_root[[:space:]]*=[[:space:]]*(.+)[[:space:]]*'
  local root="$(grep -E "^$regex\$" "$VSFTPCONFIGFILE" 2> /dev/null | \
                sed -r "s/^$regex\$/\1/" 2> /dev/null)"
  echo "$root"
}


function usage() {
  local sc="$(sanitiseScriptName "$script")"

  cat << EOF

Usage:
  $sc [option]

    --passiveports, -p  Print the passive ports, in the format "min max"
    --rootdir, -r       Print the users FTP root directory

    --help, -h          This text.

EOF
}




################################################################################
#
#
# Main
#
#
################################################################################

# store CLI
storeCLI "$(sanitiseScriptName "$script")" "$@"

# transform long options into short options
declare -i index=0
for arg in "$@"; do
  shift
  index+=1
  case "$arg" in
    "--help")
      set -- "$@" "-h"
      ;;
    "--passiveports")
      set -- "$@" "-p"
      ;;
    "--rootdir")
      set -- "$@" "-r"
      ;;
    "--"*)
      cliOptionError "$arg" $index "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
    *)
      set -- "$@" "$arg"
      ;;
  esac
done

# parse CLI
declare -i optionIndex=0
declare -i passiveOption=0
declare -i rootdirOption=0
while getopts ":hpr" option; do
  optionIndex+=1
  case "$option" in
    h) # --help
      usage
      exit 1
      ;;
    p) # --passiveports
      passiveOption=1
      ;;

    r) # --rootdir
      rootdirOption=1
      ;;
    *)
      cliOptionError "$option" $optionIndex "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))


checkRoot


if [[ ! -r "$VSFTPCONFIGFILE" ]]; then
  echo "ERROR: could not read the file '$VSFTPCONFIGFILE'"
  exit 1
fi


if [[ $passiveOption -ne 0 ]]; then
  printPassivePorts
fi

if [[ $rootdirOption -ne 0 ]]; then
  printFtpRootDir
fi


exit 0
