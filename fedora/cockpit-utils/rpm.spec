Name:             cockpit-utils
Version:          1.3.5
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Utilities for cockpit

Requires:         bash, bash-completion-pelagic-helpers
Requires:         cockpit-ws, coreutils
Requires:         firewalld
Requires:         grep
Requires:         policycoreutils-python-utils
Requires:         sed
Requires:         systemd


%description
Utilities for cockpit.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post


%files
%defattr(-,root,root)
%{_bindir}/*
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%changelog
* Sat Apr 17 2021 Ferry Huberts - 1.3.5
- Small improvements to scripts
- Improve the spec file

* Wed Feb 24 2021 Ferry Huberts - 1.3.4
- Move some bash functions to an include file

* Sun Feb 21 2021 Ferry Huberts - 1.3.3
- Rework and simplify bash completion

* Sun Feb 21 2021 Ferry Huberts - 1.3.2
- Small fixes to CLI parsing

* Sat Feb 20 2021 Ferry Huberts - 1.3.1
- Improve completion of cockpit-move-port

* Sat Feb 20 2021 Ferry Huberts - 1.3.0
- Improve usage and add bash completion

* Sun Jul 05 2020 Ferry Huberts - 1.2.10
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.2.9
- The repository moved to GitLab

* Mon Jan 01 2018 Ferry Huberts - 1.2.8
- Fix some typos and an error path

* Sun Dec 31 2017 Ferry Huberts - 1.2.7
- Bump to Fedora 27

* Mon Apr 17 2017 Ferry Huberts - 1.2.6
- Use bash' [[ builtin instead of the program [

* Fri Dec 02 2016 Ferry Huberts - 1.2.5
- Bump to Fedora 25

* Wed Dec 23 2015 Ferry Huberts - 1.2.4
- move cockpit-move-port to /usr/sbin

* Mon Dec 21 2015 Ferry Huberts - 1.2.3
- cockpit-move-port needs root privileges, add a check

* Mon Dec 21 2015 Ferry Huberts - 1.2.2
- update a dependency

* Mon Dec 21 2015 Ferry Huberts - 1.2.1
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.2.0
- refactor cockpit-move-port and change its 'install/uninstall' modes
  to 'move/restore'
- add '-d' argument to cockpit-get-port to get the default port

* Mon Dec 21 2015 Ferry Huberts - 1.1.0
- add cockpit-get-port

* Mon Dec 14 2015 Ferry Huberts - 1.0.0
- Initial release
