Name:             postfix-as-mta
Version:          1.0.18
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Use postfix as the default MTA and configure it

Requires:         coreutils
Requires:         postfix
Requires:         systemd


%description
Use postfix as the default MTA and configure it:
- Listen on all interfaces
- Maildir home mailbox
- Permit sending from unauthenticated clients on 'mynetworks'
- Permit sending from authenticated clients anywhere
- Reject unauthenticated clients not permitted by previous rules
- Enable SASL authentication
- Only allow authentication through TLS
- Configure a TLS session cache


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

alternatives \
  --set mta "/usr/sbin/sendmail.postfix"

postconf -M -e \
  'smtps/inet=smtps inet n - n - - smtpd -o syslog_name=postfix/smtps -o smtpd_tls_wrappermode=yes -o smtpd_sasl_auth_enable=yes' \
  'submission/inet=submission inet n - n - - smtpd -o syslog_name=postfix/submission -o smtpd_tls_wrappermode=yes -o smtpd_sasl_auth_enable=yes'

postconf -e \
  'inet_interfaces                  = all' \
  'home_mailbox                     = Maildir/' \
  'smtpd_sasl_path                  = private/auth' \
  'smtpd_recipient_restrictions     = permit_mynetworks, permit_sasl_authenticated, reject_unauth_destination' \
  'smtpd_sasl_auth_enable           = yes' \
  'smtpd_tls_auth_only              = yes' \
  'smtpd_tls_session_cache_database = btree:/var/lib/postfix/smtpd_tls_cache'

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "postfix.service"
fi

if [[ $1 -eq 1 ]]; then
  # install

  install -m 0700 -o "root" -g "root" -d "/root/Maildir"

  for i in "" "--permanent"; do
    firewall-cmd -q $i \
      --add-service=smtp \
      --add-service=smtps
    firewall-cmd -q $i \
      --add-port=587/tcp
  done

  systemctl -q enable            "postfix.service"
  systemctl -q reload-or-restart "postfix.service"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  alternatives \
    --auto mta

  postconf -e \
    "inet_interfaces                  = $(postconf -d -h "inet_interfaces")" \
    "home_mailbox                     = $(postconf -d -h "home_mailbox")" \
    "smtpd_sasl_path                  = $(postconf -d -h "smtpd_sasl_path")" \
    "smtpd_recipient_restrictions     = $(postconf -d -h "smtpd_recipient_restrictions")" \
    "smtpd_sasl_auth_enable           = $(postconf -d -h "smtpd_sasl_auth_enable")" \
    "smtpd_tls_auth_only              = $(postconf -d -h "smtpd_tls_auth_only")" \
    "smtpd_tls_session_cache_database = $(postconf -d -h "smtpd_tls_session_cache_database")"

  for i in "" "--permanent"; do
    firewall-cmd -q $i \
      --remove-service=smtp \
      --remove-service=smtps
    firewall-cmd -q $i \
      --remove-port=587/tcp
  done

  systemctl -q stop    "postfix.service"
  systemctl -q disable "postfix.service"
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Thu Apr 22 2021 Ferry Huberts - 1.0.18
- Finish moving enabling smtps and submission services to the postfix-as-mta
  package, the uninstall of smtpd_sasl_path was forgotten in the move.

* Wed Apr 21 2021 Ferry Huberts - 1.0.17
- Enable smtps and submission services (moved here from dovecot-on-postfix
  package)
- Also set smtpd_sasl_path

* Sun Apr 18 2021 Ferry Huberts - 1.0.16
- Improve the spec file

* Sun Apr 11 2021 Ferry Huberts - 1.0.15
- Remove changing the certificate

* Mon Apr 06 2020 Ferry Huberts - 1.0.14
- Use the localhost certificates

* Sun Apr 05 2020 Ferry Huberts - 1.0.13
- Open only the smtp firewall port (other ports are done in dovecot-on-postfix)
- Place TLS cache in a postfix directory

* Sun Apr 05 2020 Ferry Huberts - 1.0.12
- Enable smtps
- Open firewall ports on install
- Restart service after updates, when active

* Fri Mar 26 2021 Ferry Huberts - 1.0.11
- Reduce the postconf changes to account for defaults and priority.
  smtpd_tls_cert_file, smtpd_tls_key_file and smtpd_tls_loglevel will revert
  back to their defaults for new installs.

* Wed Mar 03 2021 Ferry Huberts - 1.0.10
- Move dovecot specific configuration to the dovecot-on-postfix rpm

* Wed Feb 17 2021 Ferry Huberts - 1.0.9
- Remove chkconfig as dependency

* Sun Jul 05 2020 Ferry Huberts - 1.0.8
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.7
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.6
- Bump to Fedora 27

* Fri Mar 03 2017 Ferry Huberts - 1.0.5
- Improve %post scriptlet

* Wed Dec 28 2016 Ferry Huberts - 1.0.4
- systemctl changes

* Fri Dec 23 2016 Ferry Huberts - 1.0.3
- systemctl changes

* Mon Dec 05 2016 Ferry Huberts - 1.0.2
- Bump to Fedora 25

* Sat May 21 2016 Ferry Huberts - 1.0.1
- systemctl changes

* Tue May 10 2016 Ferry Huberts - 1.0.0
- Initial release
