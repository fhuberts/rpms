Name:             bash-completion-pelagic-helpers
Version:          1.0.1
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Bash completion helpers

Requires:         bash, bash-completion


%description
Bash completion helpers.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


%files
%defattr(-,root,root)
%{_datarootdir}/bash-completion/completions/*


%changelog
* Sun Feb 21 2021 Ferry Huberts - 1.0.1
- Add _pelagic_helpers_extract function
- Require bash-completion

* Sun Feb 21 2021 Ferry Huberts - 1.0.0
- Initial release
