Name:             pki-host-certificate
Version:          1.3.5
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Regenerate the SSL host certificate every year

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils
Requires:         openssl
Requires:         systemd

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
Regenerate the SSL host certificate every year on Jan 1st at 00:00h.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "%{name}.timer"
fi

if [[ $1 -eq 1 ]]; then
  # install

  if [[ ! -f "/etc/pki/tls/private/localhost.key" ]] || \
     [[ ! -f "/etc/pki/tls/certs/localhost.crt" ]]; then
    pki-host-certificate
  fi

  systemctl -q enable            "%{name}.timer"
  systemctl -q reload-or-restart "%{name}.timer"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q stop    "%{name}.timer"
  systemctl -q disable "%{name}.timer"
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/pki/*
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*
%{_unitdir}/*


%changelog
* Tue Dec 31 2024 Ferry Huberts - 1.3.5
- Add the SAN to the certificate

* Thu Apr 29 2021 Ferry Huberts - 1.3.4
- Update the systemd unit files to not use the obsolete 'syslog' value

* Sun Apr 18 2021 Ferry Huberts - 1.3.3
- Improve the scripts
- Tighten up the timer accuracy
- Improve the spec file

* Wed Feb 24 2021 Ferry Huberts - 1.3.2
- Move some bash functions to an include file

* Sun Feb 21 2021 Ferry Huberts - 1.3.1
- Rework and simplify bash completion

* Sat Feb 20 2021 Ferry Huberts - 1.3.0
- Improve usage and add bash completion

* Sun Jul 05 2020 Ferry Huberts - 1.2.15
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.2.14
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.2.13
- Bump to Fedora 27

* Mon Apr 17 2017 Ferry Huberts - 1.2.12
- Use bash' [[ builtin instead of the program [

* Fri Mar 03 2017 Ferry Huberts - 1.2.11
- Improve scriptlets
- Increase certificate strength

* Tue Dec 13 2016 Ferry Huberts - 1.2.10
- Fix rpmlint warnings

* Mon Dec 05 2016 Ferry Huberts - 1.2.9
- Bump to Fedora 25

* Sat May 21 2016 Ferry Huberts - 1.2.8
- Fix restarting httpd after certificate generation

* Tue May 10 2016 Ferry Huberts - 1.2.7
- Run the certificate generation on install when the certificate doesn't exist

* Mon Apr 25 2016 Ferry Huberts - 1.2.6
- Fix checking the exit code of systemctl

* Wed Dec 23 2015 Ferry Huberts - 1.2.5
- fix the %%files section of the spec file

* Mon Dec 21 2015 Ferry Huberts - 1.2.4
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.2.3
- only modify services when needed

* Mon Dec 14 2015 Ferry Huberts - 1.2.2
- simplify the spec file a bit

* Mon Dec 14 2015 Ferry Huberts - 1.2.1
- add forgotten dependencies on coreutils and systemd

* Sun Dec 13 2015 Ferry Huberts - 1.2.0
- use a systemd timer instead of the cron daemon

* Sat Dec 12 2015 Ferry Huberts - 1.1.1
- cleanup scripting

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- clean up 'Requires'
- only support Fedora 23 and higher

* Wed Dec 09 2015 Ferry Huberts - 1.0.1
- run systemctl in quiet mode

* Thu Jun 25 2015 Ferry Huberts - 1.0.0-2
- Support Fedora >= 21

* Sun Jan 25 2015 Ferry Huberts - 1.0.0
- Initial release
