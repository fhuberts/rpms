#!/usr/bin/bash

set -e
set -u


if [[ "$(whoami 2> /dev/null)" != "root" ]]; then
  echo "ERROR: need to be root"
  exit 1
fi

source "/usr/share/nautilus-scripts-collection/functions.include"


################################################################################
#
#
# Settings
#
#
################################################################################

script="$0"
scriptName="$(basename "$script" 2> /dev/null)"
scriptDir="$(dirname "$script" 2> /dev/null)"
pushd "$scriptDir" &> /dev/null
scriptDir="$(pwd 2> /dev/null)"
popd &> /dev/null


distSrcDir="/usr/share/nautilus-scripts-collection/scripts"
userDstDir=".local/share/nautilus/scripts"
ifsOrg="$IFS"


################################################################################
#
#
# Functions
#
#
################################################################################

function getAllUsers() {
  getent passwd 2> /dev/null | \
  grep -Ev '/(nologin|sync|shutdown|halt)$' 2> /dev/null | \
  sort -u 2> /dev/null | \
  awk -F ':' '{ if (($3 >= 1000) && ($3 < 65000)) print $1; }' 2> /dev/null
}


function getUserHomeDir() {
  local _user="$1"

  IFS=$'\n'
  local -a _ent="$(getent passwd "$_user" 2> /dev/null)"
  IFS="$ifsOrg"

  if [[ ${#_ent[*]} -ne 1 ]]; then
    echo ""
    return
  fi

  echo "$_ent" | awk -F ':' '{ print $6; }' 2> /dev/null
}


function removeSymLinks() {
  IFS=$'\n'
  local -a symLinks=( $(find * -maxdepth 0 -type l 2> /dev/null) )
  IFS="$ifsOrg"
  local -i symLinksCount=${#symLinks[*]}

  local -i symLinkIndex=0
  while [[ $symLinkIndex -lt $symLinksCount ]]; do
    local symLink="${symLinks[$symLinkIndex]}"
    local symLinkDst="$(readlink "$symLink" 2> /dev/null)"
    local symLinkDstDir="$symLinkDst"
    if [[ ! -d "$symLinkDst" ]]; then
      symLinkDstDir="$(dirname "$symLinkDst" 2> /dev/null)"
    fi
    if [[ "$symLinkDstDir" == "$distSrcDir" ]]; then
      rm -f "$symLink"
    fi
    symLinkIndex+=1
  done
}


function addSymLinks() {
  local _user="$1"

  local -i distSrcDirFileIndex=0
  while [[ $distSrcDirFileIndex -lt $distSrcDirFilesCount ]]; do
    local distSrcDirFile="${distSrcDirFiles[$distSrcDirFileIndex]}"
    local distSrcDirFileBase="$(basename "$distSrcDirFile" 2> /dev/null)"
    rm -f "$distSrcDirFileBase"
    sudo -u "$_user" ln -sf "$distSrcDirFile" "$distSrcDirFileBase"
    distSrcDirFileIndex+=1
  done
}


function restoreSymLinksOwner() {
  local -i distSrcDirFileIndex=0
  while [[ $distSrcDirFileIndex -lt $distSrcDirFilesCount ]]; do
    local distSrcDirFile="${distSrcDirFiles[$distSrcDirFileIndex]}"
    chown root:root "$distSrcDirFile"
    distSrcDirFileIndex+=1
  done
}


function usage() {
  cat << EOF

Usage: $scriptName [option] [user]*
    --mode,-m
        add     Add the Nautilus scripts to all specified user accounts.
        remove  Remove the Nautilus scripts from all specified user accounts.
        update  Update the Nautilus scripts in all specified user accounts.
    user        One or more user account names to use. When none are
                specified then all (human) user accounts are assumed.

    --help, -h  This text.
EOF
}


################################################################################
#
#
# Main
#
#
################################################################################

# store CLI
storeCLI "$(sanitiseScriptName "$script")" "$@"

if [[ $# -eq 0 ]]; then
  echo "ERROR: specify at least the mode."
  usage
  exit 1
fi

# transform long options into short options
declare -i index=0
for arg in "$@"; do
  shift
  index+=1
  case "$arg" in
    "--help")
      set -- "$@" "-h"
      ;;
    "--mode")
      set -- "$@" "-m"
      ;;
    "--"*)
      cliOptionError "$arg" $index "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
    *)
      set -- "$@" "$arg"
      ;;
  esac
done

# parse CLI
declare -i optionIndex=0
MODE=""
while getopts ":hm:" option; do
  optionIndex+=1
  case "$option" in
    h) # --help
      usage
      exit 1
      ;;
    m) # --mode
      MODE="${OPTARG,,}"
      ;;
    *)
      cliOptionError "$option" $optionIndex "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))


if [[ -z "$MODE" ]]; then
  echo "ERROR: no mode specified."
  usage
  exit 1
fi


declare -i MODE_ADD=0
declare -i MODE_REMOVE=0
if [[ "$MODE" == "add" ]]; then
  MODE_ADD=1
  MODE_REMOVE=0
elif [[ "$MODE" == "remove" ]]; then
  MODE_ADD=0
  MODE_REMOVE=1
elif [[ "$MODE" == "update" ]]; then
  MODE_ADD=1
  MODE_REMOVE=1
else
  echo "ERROR: invalid mode."
  usage
  exit 1
fi


# get the nautilus scripts
IFS=$'\n'
declare -a distSrcDirFiles=( $(find "$distSrcDir" -type f | sort -u) )
IFS="$ifsOrg"
declare -i distSrcDirFilesCount=${#distSrcDirFiles[*]}


if [[ $# -eq 0 ]]; then
  # get all users
  IFS=$'\n'
  declare -a users=( $(getAllUsers) )
  IFS="$ifsOrg"
else
  declare -a users=( "$@" )
fi
declare -i usersCount=${#users[*]}


# loop over all users
declare -i userIndex=0
while [[ $userIndex -lt $usersCount ]]; do
  user="${users[$userIndex]}"
  userHomeDir="$(getUserHomeDir "$user")"

  if [[ -n "$userHomeDir" ]] && [[ -d "$userHomeDir" ]]; then
    pushd "$userHomeDir" &> /dev/null
    mkdir -p "$userDstDir"
    chown --reference "$userDstDir/.." "$userDstDir"
    chmod --reference "$userDstDir/.." "$userDstDir"
    cd "$userDstDir" &> /dev/null
    if [[ $MODE_REMOVE -ne 0 ]]; then
      removeSymLinks
    fi
    if [[ $MODE_ADD -ne 0 ]]; then
      addSymLinks "$user"
    fi
    popd &> /dev/null
  fi

  userIndex+=1
done

restoreSymLinksOwner

