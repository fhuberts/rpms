Name:             nautilus-scripts-collection
Version:          1.0.2
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          A collection of nautilus scripts

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils
Requires:         git, gitk, git-gui, glibc-common, grep
Requires:         kdiff3
Requires:         libnotify
Requires:         nautilus


%description
A collection of nautilus scripts.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates
nautilus-scripts-collection-update --mode update

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall
  nautilus-scripts-collection-update --mode remove
fi

exit 0


%files
%defattr(-,root,root)
%{_sbindir}/*
%{_docdir}/%{name}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%changelog
* Wed Jan 17 2024 Ferry Huberts - 1.0.2
- Fix chown syntax

* Tue Aug 16 2022 Ferry Huberts - 1.0.1
- Ensure the user's nautilus scripts directory has proper
  ownership and permissions

* Mon Dec 27 2021 Ferry Huberts - 1.0.0
- Initial release
