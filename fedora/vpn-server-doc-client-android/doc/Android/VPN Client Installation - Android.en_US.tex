\newcommand{\myTargetOS}{Android}

\newcommand{\myLanguage}{english}
\newcommand{\myPageSize}{a4paper}
\newcommand{\myDocTitle}{VPN~Client~Installation}
\newcommand{\myFooterOf}{of}
\newcommand{\myFooterSeparator}{~-~}


%-------------------------------------------------------------------------------
% Template: includes \begin{document} but not \end{document}
%-------------------------------------------------------------------------------

\input{../../vpn-server-doc.build/template.texinc}


%-------------------------------------------------------------------------------
% Introduction
%-------------------------------------------------------------------------------

\newpage
\section{Introduction}

\par
This document describes how {\myTargetOS} should be configured to be able to
establish a VPN~connection.

\par
\textbf{Note}:\\
{\myTargetOS} devices only support routed VPN connections.

\par
\textbf{Attention}:\\
The screenshots show {\myTargetOS} in English.


%-------------------------------------------------------------------------------
% Installation required software
%-------------------------------------------------------------------------------

\newpage
\section{Installation~of~Required~Software}

\par
The instructions in this chapter only need to be carried out once: all required
software will be present when an extra VPN~connection needs to be configured.


\instructionWithScreenshot{
  In the \textbf{Play~Store} search for \textbf{openvpn}.
  The \textbf{OpenVPN~Connect} app will be shown.

  Click \textbf{Install}.
}{screenshots/01.png}


\instructionWithScreenshot{
  The app is installing.
}{screenshots/02.png}


\instructionWithScreenshot{
  The installation is complete.

  Click \textbf{Open}.
}{screenshots/03.png}


\instructionWithScreenshot{
  Click \textbf{AGREE}.
}{screenshots/04.png}


\instruction{
  Exit the app.
}


\instructionWithScreenshot{
  In the \textbf{Play~Store} search for \textbf{rar}.
  The \textbf{RAR} app will be shown.

  Click \textbf{Install}.
}{screenshots/05.png}


\instructionWithScreenshot{
  The app is installing.
}{screenshots/06.png}


\instructionWithScreenshot{
  The installation is complete.

  Click \textbf{Open}.
}{screenshots/07.png}


\instructionWithScreenshot{
  The grey notice asks for access to all files for the RAR app.
}{screenshots/08.png}


\instructionWithExtra{
  Enable access to all files for the RAR app.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/09.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (3.1,1.4) -- (6.1,1.4);
    \end{tikzpicture}
  \end{center}
}


\instruction{
  Any time a 'Support RAR development' pop-up appears, just click
  \textbf{Dismiss}.
}


\instruction{
  Exit the app.
}


%-------------------------------------------------------------------------------
% VPN Certificates
%-------------------------------------------------------------------------------

\newpage
\section{VPN~Certificates}

\par
The VPN~server administrator sent an email with an attached ZIP~file.
This ZIP~file contains the configuration files for the VPN~connection.


\instruction{
  Save the ZIP~file (for example in the \textbf{Downloads} folder).
}


\instruction{
  Open the \textbf{RAR} app.
}


\instructionWithScreenshot{
  Navigate to the folder where the ZIP~file resides (\textbf{Downloads}), and
  click it.
}{screenshots/10.png}


\instructionWithExtra{
  The contents of the zip file are shown.

  Click the \textbf{Extract} icon.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/11.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (11.05,3.4) -- (11.05,6.4);
    \end{tikzpicture}
  \end{center}
}


\instructionWithScreenshot{
  The \textbf{Extract~options} dialog is shown.

  Change the destination path by clicking on it and editing it.
  Use a path like the one that is shown.

  This is recommended because the VPN certificates must remain on the device
  to be able to use the VPN, and \textbf{Downloads} isn't a good place for
  these files.

  Click \textbf{Ok}.
}{screenshots/12.png}


\instructionWithScreenshot{
  The \textbf{Enter~password} dialog is shown. A password is required because
  the ZIP~file is encrypted.

  Enter the password and click \textbf{Ok}.
}{screenshots/13.png}


\instruction{
  Exit the app.
}


%-------------------------------------------------------------------------------
% Connection Configuration
%-------------------------------------------------------------------------------

\newpage
\section{Connection~Configuration}

\instructionWithScreenshot{
  Open the \textbf{OpenVPN~Connect} app.

  Since there are no profiles defined yet, the app will open with the
  \textbf{Import~Profile} dialog.

  Click on the \textbf{FILE} tab.
}{screenshots/14.png}


\instructionWithScreenshot{
  Click \textbf{Browse}.
}{screenshots/15.png}


\instructionWithScreenshot{
  Click the hamburger menu in the top-left corner.
}{screenshots/16.png}


\instructionWithScreenshot{
  Navigate to the folder where the extracted VPN configuration files reside
  (\textbf{Internal~Storage/vpn/exampleinc-client}).

  Select the desired profile file.
}{screenshots/17.png}


\instructionWithScreenshot{
  Click \textbf{OK}.
}{screenshots/18.png}


\instructionWithScreenshot{
  Adjust the \textbf{Profile Name}.

  Click \textbf{Add} in the upper-right corner.
}{screenshots/19.png}


\instruction {
  Repeat this procedure for each of the other '\textbf{ovpn}' files, as desired
  (click the orange 'plus' button in the lower-right corner).
}


\instructionWithScreenshot{
  The VPN~connection configuration is complete.
}{screenshots/20.png}


%-------------------------------------------------------------------------------
% Connecting
%-------------------------------------------------------------------------------

\newpage
\section{Connecting}

\par
The VPN~connection can be established in a number of ways.

\par
Always follow the list of options as shown below when establishing a
VPN~connection and when doing so choose the first option that works. Only choose
the next option when the chosen option doesn't work.
\begin{enumerate}
  \item \mbox{UDP~-~Routed}
  \item \mbox{TCP~-~Routed}
\end{enumerate}

\par
The UDP options are preferred over the TCP options since they deliver much
more performance. The reason for that is that the UDP options allow the network
stacks on the client and server systems to perform proper window scaling and
such.

\newpage
\instructionWithScreenshot{
  Open the \textbf{OpenVPN~Connect} app.

  Click the desired \textbf{OpenVPN~Profile} to start the connection.
}{screenshots/20.png}


\instructionWithScreenshot{
  Click \textbf{OK} on the \textbf{Connection~request} dialog.
}{screenshots/21.png}


\instructionWithScreenshot{
  The VPN~connection has been established.
}{screenshots/22.png}


%-------------------------------------------------------------------------------
% Disconnecting
%-------------------------------------------------------------------------------

\newpage
\section{Disconnecting}


\instructionWithScreenshot{
  Open the \textbf{OpenVPN~Connect} app.

  Click the active \textbf{OpenVPN~Profile} to disconnect.
}{screenshots/23.png}


\instructionWithScreenshot{
  Check the \textbf{Don't~show~again} option, and click \textbf{OK}.
}{screenshots/24.png}


\instructionWithScreenshot{
  The VPN~connection has been disconnected.
}{screenshots/25.png}


%-------------------------------------------------------------------------------
% End of document
%-------------------------------------------------------------------------------

\end{document}
