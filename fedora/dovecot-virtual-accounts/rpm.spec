%global dovecotUsersFile  %{_sysconfdir}/dovecot/users

Name:             dovecot-virtual-accounts
Version:          1.1.1
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Enable dovecot virtual accounts, with helper script

Requires:         bash-completion-pelagic-helpers
Requires:         dovecot-on-postfix

Requires:         bash
Requires:         coreutils
Requires:         dovecot
Requires:         gawk, grep
Requires:         glibc-common
Requires:         shadow-utils
Requires:         sed, systemd


%description
Enable dovecot virtual accounts, with helper script.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

systemctl -q try-reload-or-restart "dovecot.service"

if [[ $1 -ne 1 ]]; then
  exit 0
fi

# install

exit 0


%postun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart "dovecot.service"
fi

exit 0


%files
%defattr(-,root,root)
%config %{_sysconfdir}/dovecot/conf.d/*
%config(noreplace) %attr(640,dovecot,root) %{dovecotUsersFile}
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%changelog
* Wed Jan 17 2024 Ferry Huberts - 1.1.1
- Fix chown syntax

* Sun Nov 28 2021 Ferry Huberts - 1.1
- Add --change-password option to dovecot-virtual-accounts

* Sun Apr 18 2021 Ferry Huberts - 1.0.1
- Improve the spec file

* Sat Apr 10 2020 Ferry Huberts - 1.0.0
- Initial release
