Name:             rpm-server
Version:          2.5
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Automatically create DNF Repositories after an FTP upload

Requires:         ftp-server

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils, createrepo_c
Requires:         findutils, firewalld
Requires:         glibc-common
Requires:         httpd
Requires:         inotify-tools
Requires:         libappstream-glib-builder
Requires:         policycoreutils-python-utils
Requires:         shadow-utils, systemd
Requires:         util-linux

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
Automatically create DNF Repositories after an FTP upload.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build
find ".%{_mandir}" -type f -exec gzip '{}' \;


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  # adjust SELinux
  setsebool -P httpd_read_user_content=true

  for i in "" "--permanent"; do
    firewall-cmd -q $i --add-service=http
  done

  systemctl -q enable            "httpd.service"
  systemctl -q reload-or-restart "httpd.service"
fi

exit 0


%preun


%files
%defattr(-,root,root)
%{_bindir}/*
%{_sbindir}/*
%{_unitdir}/*
%lang(en) %{_mandir}/en/*/*.gz
%{_sysconfdir}/%{name}/rpm-server-indexer.d/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%changelog
* Sun Aug 28 2022 Ferry Huberts - 2.5
- Improve waiting for trigger files

* Tue May 03 2022 Ferry Huberts - 2.4.1
- Fix the systemd service file

* Wed Jun 16 2021 Ferry Huberts - 2.4
- No longer use a systemd path: it runs into the SELinux policy since systemd
  is not allowed to watch a user's directory

* Sun Jun 13 2021 Ferry Huberts - 2.3.3
- Make the indexer more robust

* Thu Apr 29 2021 Ferry Huberts - 2.3.2
- Remove the service/path upgrade from the spec file, it doesn't work properly
- Make sure to handle both the systemd path and service
- Update the systemd unit files to not use the obsolete 'syslog' value

* Mon Apr 19 2021 Ferry Huberts - 2.3.1
- Improve the spec file

* Wed Feb 24 2021 Ferry Huberts - 2.3.0
- Improve usage and add bash completion

* Mon Feb 22 2021 Ferry Huberts - 2.2.0
- Add 'hooks' facility to the indexer

* Sun Jul 05 2020 Ferry Huberts - 2.1.1
- Bump to Fedora 32

* Wed Nov 27 2019 Ferry Huberts - 2.1
- Use a systemd path to trigger the indexer

* Mon Jul 08 2019 Ferry Huberts - 2.0.12
- createrepo_c problem was fixed, use it again
  https://bugzilla.redhat.com/show_bug.cgi?id=1714666

* Tue Jul 02 2019 Ferry Huberts - 2.0.11
- createrepo_c has a problem, use createrepo again

* Wed Jun 06 2018 Ferry Huberts - 2.0.10
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 2.0.9
- Bump to Fedora 27
- Fix some typos

* Mon Apr 17 2017 Ferry Huberts - 2.0.8
- Use bash' [[ builtin instead of the program [

* Wed Mar 01 2017 Ferry Huberts - 2.0.7
- Improve %post scriptlet
- Remove %preun scriptlet
- Add man pages
- Do more checks when adding or removing a user

* Wed Mar 01 2017 Ferry Huberts - 2.0.6
- Enable appstream again, works after update
- Add a check

* Tue Feb 28 2017 Ferry Huberts - 2.0.5
- Disable appstream for now, it crashes

* Wed Dec 28 2016 Ferry Huberts - 2.0.4
- systemctl changes

* Tue Dec 13 2016 Ferry Huberts - 2.0.3
- Fix rpmlint warnings

* Tue Dec 06 2016 Ferry Huberts - 2.0.2
- Bump to Fedora 25

* Wed Apr 27 2016 Ferry Huberts - 2.0.1
- Fix crash when an appstream file is not generated

* Wed Apr 27 2016 Ferry Huberts - 2.0.0
- Add appstream support

* Wed Apr 27 2016 Ferry Huberts - 1.2.0
- Use simple filenames in the repodata directory

* Wed Apr 27 2016 Ferry Huberts - 1.1.0
- Use createrepo_c; it is faster

* Wed Apr 27 2016 Ferry Huberts - 1.0.9
- Do not create a database in the index directory; only used by yum, which
  is obsolete.

* Mon Apr 25 2016 Ferry Huberts - 1.0.8
- Fix checking the exit code of systemctl

* Wed Dec 23 2015 Ferry Huberts - 1.0.7
- only adjust SELinux booleans when needed

* Mon Dec 21 2015 Ferry Huberts - 1.0.6
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.0.5
- only modify services when needed

* Mon Dec 14 2015 Ferry Huberts - 1.0.4
- redirect error output of find commands to /dev/null

* Sat Dec 12 2015 Ferry Huberts - 1.0.3
- cleanup scripting

* Fri Dec 11 2015 Ferry Huberts - 1.0.2
- use quiet mode for firewall-cmd

* Thu Dec 10 2015 Ferry Huberts - 1.0.1
- rpm-server-add-user: fix remove mode

* Thu Dec 10 2015 Ferry Huberts - 1.0.0
- Initial release
