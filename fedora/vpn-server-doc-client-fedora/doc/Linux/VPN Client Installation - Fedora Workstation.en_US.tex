\newcommand{\myTargetOS}{Fedora~Workstation}

\newcommand{\myLanguage}{english}
\newcommand{\myPageSize}{a4paper}
\newcommand{\myDocTitle}{VPN~Client~Installation}
\newcommand{\myFooterOf}{of}
\newcommand{\myFooterSeparator}{~-~}


%-------------------------------------------------------------------------------
% Template: includes \begin{document} but not \end{document}
%-------------------------------------------------------------------------------

\input{../../vpn-server-doc.build/template.texinc}


%-------------------------------------------------------------------------------
% Introduction
%-------------------------------------------------------------------------------

\newpage
\section{Introduction}

\par
This document describes how {\myTargetOS} should be configured to be able to
establish a VPN~connection.

\par
\textbf{Attention}:\\
The screenshots show {\myTargetOS} in English.


%-------------------------------------------------------------------------------
% Software Installation
%-------------------------------------------------------------------------------

\newpage
\section{Software~Installation}

\par
All required software is already installed.


%-------------------------------------------------------------------------------
% Connection Configuration
%-------------------------------------------------------------------------------

\newpage
\section{Connection~Configuration}

\par
The VPN~server administrator sent an email with an attached ZIP~file that
contains the configuration files for the VPN~connection.


\instructionWithScreenshot{
  Save the ZIP~file (for example in the \textbf{Downloads} folder).

  Click \textbf{Activities} in the \mbox{upper-left} corner of the screen.
  The \textbf{Activities~overview} is shown.

  Type '\textbf{files}', click the '\textbf{Files}' icon and navigate to
  the folder where the ZIP~file resides (\textbf{Downloads}).
}{screenshots/01.png}


\instructionWithScreenshot{
  \mbox{Right-click} the ZIP~file and select \textbf{Extract}.
}{screenshots/02.png}


\instructionWithScreenshot{
  The ZIP~file is encrypted: a password is required to be able to unpack it.

  Enter the password and click \textbf{Extract}.
}{screenshots/03.png}


\instructionWithExtra{
  An extra folder is shown in the existing \textbf{Files} window. This extra
  folder contains the unpacked files.

  Click one of the icons in the \mbox{upper-right} corner of the screen to
  open the status menu. Then click the \textbf{gear} icon to open the
  \textbf{Settings} window.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/04.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (8.6,5.2) -- (8.6,8.2);
    \end{tikzpicture}
  \end{center}
}


\instructionWithScreenshot{
  Click the \textbf{Network} category on the left.
}{screenshots/05.png}


\instructionWithExtra{
  The \textbf{Network} window is shown. Click the plus icon (\textbf{+}) next
  to \textbf{VPN}.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/06.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (7.9,6.45) -- (10.9,6.45);
    \end{tikzpicture}
  \end{center}
}


\instructionWithExtra{
  Click \textbf{Import~from~file\ldots}
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/07.png}
      };
      \draw[yellow, line width=4pt, <-, >= triangle 45] (4.7,2.2) -- (7.7,2.2);
    \end{tikzpicture}
  \end{center}
}


\instructionWithScreenshot{
  A \textbf{Select~file~to~import} window is shown.

  The VPN~configuration files all reside in the extra folder that was created
  when unpacking the ZIP~file.

  Navigate to that extra folder and select an '\textbf{ovpn}' file by
  \mbox{double-clicking} it.

  There can be one or more '\textbf{ovpn}' files, depending on what the
  VPN~administrator decided. Each of these files contains settings to establish
  the VPN~connection in a certain way.
}{screenshots/08.png}


\instructionWithScreenshot{
  An \textbf{Add~VPN} window is shown with the settings from the
  selected '\textbf{ovpn}' file.
}{screenshots/09.png}


\instruction {
  Click the \textbf{Add} button in the \mbox{upper-right} corner of the
  \textbf{Add~VPN} window.
}


\instructionWithScreenshot{
  The VPN~connection configuration is complete.
}{screenshots/10.png}


\instruction {
  Repeat this procedure for each of the other '\textbf{ovpn}' files, as desired.
}


\instruction {
  Close the \textbf{Settings} window by clicking the '\textbf{x}' in the
  upper-right corner of the window.
}


\instructionWithScreenshot{
  Remove the folder with the unpacked files.

  \mbox{Right-click} the extra folder and select \textbf{Move to Trash}.

  The folder with the unpacked files contains sensitive information (the
  VPN~connection certificates) that must not come into possession of a third
  party.
}{screenshots/11.png}


\instruction{
  Close the \textbf{Files} window by clicking the '\textbf{x}' in the
  upper-right corner of the window.
}


%-------------------------------------------------------------------------------
% Connecting
%-------------------------------------------------------------------------------

\newpage
\section{Connecting}

\par
The VPN~connection can be established in a number of ways.

\par
Always follow the list of options as shown below when establishing a
VPN~connection and when doing so choose the first option that works. Only choose
the next option when the chosen option doesn't work.
\begin{enumerate}
  \item \mbox{UDP~-~Routed}
  \item \mbox{TCP~-~Routed}
  \item \mbox{UDP~-~Bridged}
  \item \mbox{TCP~-~Bridged}
\end{enumerate}

\par
The routed options are preferred over the bridged options since the routed
options operate on the IP protocol level of the network stack and therefore use
less bandwidth (as opposed to the bridged options that operate on the Ethernet
level of the network stack).

\par
The UDP options are preferred over the TCP options since they deliver much
more performance. The reason for that is that the UDP options allow the network
stacks on the client and server systems to perform proper window scaling.


\newpage
\instructionWithScreenshot{
  Click one of the icons in the \mbox{upper-right} corner of the screen to
  open the status menu.

  Then click the '\textbf{\textgreater}' on the right of the
  '\textbf{Example In...}' with the lock symbol before it (symbolising the
  VPN connections).

  Click the chosen VPN~connection.
}{screenshots/12.png}


\instructionWithExtra{
  The VPN~connection is being established. This is indicated by the lock icon
  with dots.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/13.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (11.5,6) -- (11.5,9);
    \end{tikzpicture}
  \end{center}
}


\instructionWithExtra{
  The VPN~connection has been established. This is indicated by the lock icon.
}{
  \begin{center}
    \begin{tikzpicture}
      \node[anchor=south west,inner sep=0] at (0,0) {
        \includegraphics[width=\myScreenshotSize\textwidth]{screenshots/14.png}
      };
      \draw[yellow, line width=4pt, ->, >= triangle 45] (11.5,6) -- (11.5,9);
    \end{tikzpicture}
  \end{center}
}


%-------------------------------------------------------------------------------
% Disconnecting
%-------------------------------------------------------------------------------

\newpage
\section{Disconnecting}


\instructionWithScreenshot{
  Click one of the icons in the \mbox{upper-right} corner of the screen to
  open the status menu.

  Then click the '\textbf{\textgreater}' on the right of the
  '\textbf{Example In...}' with the lock symbol before it (symbolising the
  VPN connections).

  Click the name of the active VPN~connection
  (\textbf{\mbox{Example~Inc.~-~UDP~-~Routed}} in the screenshot).
}{screenshots/15.png}


\instructionWithScreenshot{
  The VPN~connection is disconnected. This is indicated by the absence of the
  lock icon.
}{screenshots/16.png}


%-------------------------------------------------------------------------------
% End of document
%-------------------------------------------------------------------------------

\end{document}
