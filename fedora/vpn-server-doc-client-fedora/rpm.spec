%include ../vpn-server-doc.build/spec-base.include

%global _summary Fedora client installation manuals for %{basepackage}

Name:             %{basepackage}-doc-client-fedora
Version:          %{_version}
Release:          %{_release}%{?dist}

%include ../vpn-server-doc.build/spec.include


%description
%{_summary}


%package          en-US
Summary:          %{_summary} - US English files
%description      en-US
%{_summary}


%package          nl-NL
Summary:          %{_summary} - Dutch files
%description      nl-NL
%{_summary}


%files en-US
%defattr(-,root,root)
%lang(en) %{_datarootdir}/%{basepackage}/manuals/user/*/*.en_US.*


%files nl-NL
%defattr(-,root,root)
%lang(nl) %{_datarootdir}/%{basepackage}/manuals/user/*/*.nl_NL.*


%changelog
%{_changelog}
