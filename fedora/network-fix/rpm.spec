Name:             apcupsd-network-fix
Version:          1.0.10
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Restart apcupsd when the network changes

Requires:         apcupsd
Requires:         bash
Requires:         NetworkManager
Requires:         systemd


%package -n ddclient-network-fix
Summary:          Restart ddclient when the network changes

Requires:         ddclient
Requires:         bash
Requires:         NetworkManager
Requires:         systemd


%description
Restart apcupsd when the network changes.


%description -n ddclient-network-fix
Restart ddclient when the network changes.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install
  systemctl -q enable                "NetworkManager-dispatcher.service"
  systemctl -q reload-or-restart     "NetworkManager-dispatcher.service"

  systemctl -q try-reload-or-restart "NetworkManager.service"
fi

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart \
    "NetworkManager-dispatcher.service" \
    "NetworkManager.service"
fi

exit 0


%postun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart \
    "NetworkManager-dispatcher.service" \
    "NetworkManager.service"
fi

exit 0


%post -n ddclient-network-fix

# install and updates

if [[ $1 -eq 1 ]]; then
  # install
  systemctl -q enable                "NetworkManager-dispatcher.service"
  systemctl -q reload-or-restart     "NetworkManager-dispatcher.service"

  systemctl -q try-reload-or-restart "NetworkManager.service"
fi

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart \
    "NetworkManager-dispatcher.service" \
    "NetworkManager.service"
fi

exit 0


%postun -n ddclient-network-fix

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q try-reload-or-restart \
    "NetworkManager-dispatcher.service" \
    "NetworkManager.service"
fi

exit 0


%files
%defattr(-,root,root)
%attr(755, root, root) %{_sysconfdir}/NetworkManager/dispatcher.d/99-apcupsd


%files -n ddclient-network-fix
%defattr(-,root,root)
%attr(755, root, root) %{_sysconfdir}/NetworkManager/dispatcher.d/99-ddclient


%changelog
* Wed Aug 10 2022 Ferry Huberts - 1.0.10
- Slow down restarting services, otherwise the restart rate limit is hit

* Wed Apr 21 2021 Ferry Huberts - 1.0.9
- Add the ddclient-network-fix package

* Sat Apr 17 2021 Ferry Huberts - 1.0.8
- Update and improve the dispatcher script and the spec file
- Tweak starting services

* Sun Jul 05 2020 Ferry Huberts - 1.0.7
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.6
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.5
- Bump to Fedora 27
- Make sure NetworkManager-dispatcher is started when needed

* Mon Apr 17 2017 Ferry Huberts - 1.0.4
- Use bash' [[ builtin instead of the program [

* Wed Mar 01 2017 Ferry Huberts - 1.0.3
- Add %post and %postun scriptlets
- Improve dispatcher script

* Wed Dec 28 2016 Ferry Huberts - 1.0.2
- Minor script fix

* Fri Dec 02 2016 Ferry Huberts - 1.0.1
- Bump to Fedora 25

* Thu Oct 06 2016 Ferry Huberts - 1.0.0
- Initial release
