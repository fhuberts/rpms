%global muttrc      %{_sysconfdir}/Muttrc.local
%global muttMacros  %{_sysconfdir}/mutt/Macros

Name:             mutt-macros
Version:          1.1.9
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Mutt macros

Requires:         coreutils
Requires:         grep
Requires:         mutt
Requires:         sed


%description
A configuration file is added to the system-wide mutt configuration
files to add some useful macros.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  if [[ ! -f "%{muttrc}" ]]; then
    touch "%{muttrc}"
  fi

  regex="^[[:space:]]*source[[:space:]]+%{muttMacros}[[:space:]]*\$"
  grepped="$(grep -Ei "$regex" "%{muttrc}")"
  if [[ -z "$grepped" ]]; then
    echo "source %{muttMacros}" >> "%{muttrc}"
  fi
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  if [[ -f "%{muttrc}" ]]; then
    regex="^[[:space:]]*source[[:space:]]+%{muttMacros}[[:space:]]*\$"
    regexEscaped="${regex//\//\\/}"
    sed -r -i "/$regexEscaped/ d" "%{muttrc}"
  fi
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/mutt/*


%changelog
* Sun Apr 18 2021 Ferry Huberts - 1.1.9
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.1.8
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.1.7
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.1.6
- Bump to Fedora 27

* Fri Mar 03 2017 Ferry Huberts - 1.1.5
- Improve scriptlets

* Wed Jan 11 2017 Ferry Huberts - 1.1.4
- Fix post-install and pre-uninstall scripts

* Wed Dec 14 2016 Ferry Huberts - 1.1.3
- Update 'Requires'

* Fri Dec 02 2016 Ferry Huberts - 1.1.2
- Bump to Fedora 25

* Sat Dec 12 2015 Ferry Huberts - 1.1.1
- improve package removal

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- clean up 'Requires'
- only support Fedora 23 and higher

* Sun Jan 25 2015 Ferry Huberts - 1.0.0
- Initial release
