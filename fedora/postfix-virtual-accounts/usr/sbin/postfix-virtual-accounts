#!/usr/bin/bash

set -e
set -u

script="$0"

source "/usr/share/postfix-virtual-accounts/functions.include"




#
# Functions
#

function usage() {
  local __sc="$(sanitiseScriptName "$script")"

  cat << EOF

Usage:
  $__sc [option]

    --add-domain   , -d <domain>
    --remove-domain, -D <domain>
        Add or remove a virtual email domain, like 'example.com'.

    --add-domain-catch-all   , -c <domain>=<user@todomain>
    --remove-domain-catch-all, -C <domain>
        Add or remove a catch-all rule for a virtual email domain, like
        'example.com'. A catch-all rule routes all email destinations in the
        specified domain that are not caught by explicit user destinations
        to a specific destination. There can only be one such rule per virtual
        email domain. A new rule will overwrite an existing rule.

    --add-user, -u <user@domain>
    --remove-user, -U <user@domain>
        Add or remove a user account for a virtual email domain, like
        'user@example.com'.
        When a user account is removed, the email is moved to directory
        $vmailDirBackup.

    --add-alias, -a <fromuser@fromdomain>=<touser@todomain>
    --remove-alias, -A <fromuser@fromdomain>
        Add or remove an alias for a virtual email user. A new alias will
        overwrite an existing rule.
        For example:
        company@sistercompany.com=sistercompany@company.com will route all
        email addressed to company@sistercompany.com to
        sistercompany@company.com.

    --list-all       , -l
    --list-domains   , -m
    --list-users     , -n
    --list-catch-alls, -o
    --list-aliases   , -p
        List the specified items.

    --help, -h
        This text.

EOF
}


function addDomain() {
  local __domain="$1" # <domain>

  if [[ $(isDomainPresent "$__domain") -ne 0 ]]; then
    # domain is already present
    return
  fi

  # domain is not yet present

  createVmailDirectories "$(getVhostDomainDirectoryAbsolute "$__domain")"

  echo "$__domain" >> "$vhostsPostfix"

  doPostfixRestart=1
}


function removeDomain() {
  local __domain="$1" # <domain>

  if [[ $(isDomainPresent "$__domain") -eq 0 ]]; then
    # domain is not present
    return
  fi

  # domain is present

  # the domain must not be in use (neither by email addresses nor by catch-all)
  if [[ -n "$(getFileWithoutComments "$vmailboxPostfix" | \
              grep -E "^[[:space:]]*[^@]*@$__domain[[:space:]]+.+[[:space:]]*\$" 2> /dev/null)" ]]; then
    echo "ERROR: can't remove domain '$__domain' since it's still being used." > /dev/stderr
    exitCode=1
    return
  fi

  systemctl -q stop "postfix.service"

  sed -ri "/^[[:space:]]*$__domain[[:space:]]*\$/ d" "$vhostsPostfix" 2> /dev/null

  removeVmailDomainDirectory "$(getVhostDomainDirectoryAbsolute "$__domain")"

  doPostfixRestart=1
}


function addDomainCatchAll() {
  local __emailCatchAll="$1" # <domain>=<user@todomain>

  local __ifsOrg="$IFS"
  IFS='='
  local -a __split=( $__emailCatchAll )
  IFS="$__ifsOrg"
  local __domain="${__split[0]}"
  local __emailAddress="${__split[1]}"

  # domain must be present
  if [[ $(isDomainPresent "$__domain") -eq 0 ]]; then
    echo "ERROR: domain '$__domain' is not present, not adding a catch-all for it." > /dev/stderr
    exitCode=1
    return
  fi

  # emailAddress must be present
  if [[ $(isEmailAddressPresent "$__emailAddress") -eq 0 ]]; then
    echo "ERROR: user account '$__emailAddress' is not present, not adding a catch-all for domain '$__domain'." > /dev/stderr
    exitCode=1
    return
  fi

  # overwrite if already present

  removeDomainCatchAll "$__domain"
  echo "@$__domain $(getVhostEmailAddressDirectoryRelative "$__emailAddress")" >> "$vmailboxPostfix"

  ensureCatchAllAtEndOfFile "$vmailboxPostfix"

  doPostmap["$vmailboxPostfix"]="1"
  doPostfixRestart=1
}


function removeDomainCatchAll() {
  local __domain="$1" # <domain>

  if [[ $(isDomainCatchAllPresent "$__domain") -eq 0 ]]; then
    # domain catch-all is not present
    return
  fi

  # domain catch-all is present

  sed -ri "/^[[:space:]]*@$__domain[[:space:]]+.+\$/ d" "$vmailboxPostfix" 2> /dev/null

  doPostmap["$vmailboxPostfix"]="1"
  doPostfixRestart=1
}


function addEmailUser() {
  local __emailAddress="$1" # <user@domain>

  if [[ $(isEmailAddressPresent "$__emailAddress") -ne 0 ]]; then
    # user account is already present
    return
  fi

  # emailAddress is not present

  local __ifsOrg="$IFS"
  IFS='@'
  local -a __split=( $__emailAddress )
  IFS="$__ifsOrg"
  local __domain="${__split[1]}"

  # domain must be present
  if [[ $(isDomainPresent "$__domain") -eq 0 ]]; then
    echo "ERROR: domain '$__domain' is not present, not adding user account '$__emailAddress'." > /dev/stderr
    exitCode=1
    return
  fi

  local __emailDirAbsolute="$(getVhostEmailAddressDirectory "$__emailAddress")"
  local __emailDirRelative="$(getVhostEmailAddressDirectoryRelative "$__emailAddress")"

  createVmailDirectories \
    "$__emailDirAbsolute" \
    "$__emailDirAbsolute/.Archives" \
    "$__emailDirAbsolute/.Drafts" \
    "$__emailDirAbsolute/.Junk" \
    "$__emailDirAbsolute/.Sent" \
    "$__emailDirAbsolute/.Templates" \
    "$__emailDirAbsolute/.Trash"
  echo "$__emailAddress $__emailDirRelative" >> "$vmailboxPostfix"

  ensureCatchAllAtEndOfFile "$vmailboxPostfix"

  doPostmap["$vmailboxPostfix"]="1"
  doPostfixRestart=1
}


function removeEmailUser() {
  local __emailAddress="$1" # <user@domain>

  if [[ $(isEmailAddressPresent "$__emailAddress") -eq 0 ]]; then
    # user account is not present
    return
  fi

  # emailAddress is present

  local __ifsOrg="$IFS"
  IFS='@'
  local -a __split=( $__emailAddress )
  IFS="$__ifsOrg"
  local __domain="${__split[1]}"

  # emailAddress and domain catch-all may not use the same directory
  if [[ $(isDomainCatchAllPresent "$__domain") -ne 0 ]]; then
    local __regexCatchAll="^[[:space:]]*@$__domain[[:space:]]+(.+)[[:space:]]*\$"
    local __catchAllDir="$(getFileWithoutComments "$vmailboxPostfix" | \
                         grep -Ei "$__regexCatchAll" 2> /dev/null | \
                         sed -r "s#$__regexCatchAll#\1#" 2> /dev/null)"
    local __regexUser="^[[:space:]]*$__emailAddress[[:space:]]+(.+)[[:space:]]*\$"
    local __userDir="$(getFileWithoutComments "$vmailboxPostfix" | \
                     grep -Ei "$__regexUser" 2> /dev/null | \
                     sed -r "s#$__regexUser#\1#" 2> /dev/null)"
    if [[ "$__catchAllDir" == "$__userDir" ]]; then
      echo "ERROR: user account '$__emailAddress' is in use by its domain catch-all, not removing the user account." > /dev/stderr
      exitCode=1
      return
    fi
  fi

  # emailAddress may not be in use by an alias
  if [[ $(isAliasDestination "$__emailAddress") -ne 0 ]]; then
    echo "ERROR: user account '$__emailAddress' is in use by an alias, not removing the user account." > /dev/stderr
    exitCode=1
    return
  fi

  systemctl -q stop "postfix.service"

  local __domainDirBackup="$(getVhostDomainDirectoryAbsoluteBackup "$__domain")"
  createVmailDirectories "$__domainDirBackup"

  local __emailDir="$(getVhostEmailAddressDirectory "$__emailAddress")"
  timestampAndMoveDirectory "$__emailDir" "$__domainDirBackup"

  sed -ri "/^[[:space:]]*$__emailAddress[[:space:]]+.+\$/ d" "$vmailboxPostfix" 2> /dev/null

  doPostmap["$vmailboxPostfix"]="1"
  doPostfixRestart=1
}


function addEmailAlias() {
  local __alias="$1" # <fromuser@fromdomain>=<touser@todomain>

  local __ifsOrg="$IFS"
  IFS='='
  local -a __split=( $__alias )
  IFS="$__ifsOrg"
  local __emailAddressSrc="${__split[0]}"
  local __emailAddressDst="${__split[1]}"

  # alias target must be present
  if [[ $(isEmailAddressPresent "$__emailAddressDst") -eq 0 ]]; then
    echo "ERROR: user account '$__emailAddressDst' is not present, not adding an alias from '$__emailAddressSrc' to it." > /dev/stderr
    exitCode=1
    return
  fi

  # overwrite if already present

  removeEmailAlias "$__emailAddressSrc"
  echo "$__emailAddressSrc $__emailAddressDst" >> "$virtualPostfix"

  doPostmap["$virtualPostfix"]="1"
  doPostfixRestart=1
}


function removeEmailAlias() {
  local __emailAddress="$1" # <fromuser@fromdomain>

  sed -ri "/^[[:space:]]*$__emailAddress[[:space:]]+.+\$/ d" "$virtualPostfix" 2> /dev/null

  doPostmap["$virtualPostfix"]="1"
  doPostfixRestart=1
}


function listDomains() {
  listSortedFile "$vhostsPostfix"
}


function listUsers() {
  listSortedFile "$vmailboxPostfix" | \
    grep -Ev '^[[:space:]]*@.+$' 2> /dev/null
}


function listCatchAlls() {
  listSortedFile "$vmailboxPostfix" | \
    grep -E  '^[[:space:]]*@.+$' 2> /dev/null
}


function listAliases() {
  listSortedFile "$virtualPostfix"
}




#
# Main
#

checkRoot
checkVmailAccountExists

set +e
systemctl -q is-active "postfix.service"
declare -i postfixActiveOnStart=$?
set -e
if [[ $postfixActiveOnStart -eq 0 ]]; then
  postfixActiveOnStart=1
else
  postfixActiveOnStart=0
fi

declare -i exitCode=0
declare -i doPostfixRestart=0
declare -A doPostmap=()


# store CLI
storeCLI "$(sanitiseScriptName "$script")" "$@"

# transform long options into short options
declare -i index=0
for arg in "$@"; do
  shift
  index+=1
  case "$arg" in
    "--help")
      set -- "$@" "-h"
      ;;
    "--add-domain")
      set -- "$@" "-d"
      ;;
    "--remove-domain")
      set -- "$@" "-D"
      ;;
    "--add-domain-catch-all")
      set -- "$@" "-c"
      ;;
    "--remove-domain-catch-all")
      set -- "$@" "-C"
      ;;
    "--add-user")
      set -- "$@" "-u"
      ;;
    "--remove-user")
      set -- "$@" "-U"
      ;;
    "--add-alias")
      set -- "$@" "-a"
      ;;
    "--remove-alias")
      set -- "$@" "-A"
      ;;
    "--list-all")
      set -- "$@" "-l"
      ;;
    "--list-domains")
      set -- "$@" "-m"
      ;;
    "--list-users")
      set -- "$@" "-n"
      ;;
    "--list-catch-alls")
      set -- "$@" "-o"
      ;;
    "--list-aliases")
      set -- "$@" "-p"
      ;;
    "--"*)
      cliOptionError "$arg" $index "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
    *)
      set -- "$@" "$arg"
      ;;
  esac
done


# parse CLI
declare -i optionIndex=0
while getopts ":hd:D:c:C:u:U:a:A:lmnop" option; do
  arg="$(stringTrim "${OPTARG:-,,}")"
  optionIndex+=1
  case "$option" in
    h) # --help
      usage
      exit 1
      ;;
    d) # --add-domain
      __r=""
      checkEmailDomain "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      addDomain "$arg"
      ;;
    D) # --remove-domain
      __r=""
      checkEmailDomain "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      removeDomain "$arg"
      ;;
    c) # --add-domain-catch-all
      __r=""
      checkEmailDomainCatchAll "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      addDomainCatchAll "$arg"
      ;;
    C) # --remove-domain-catch-all
      __r=""
      checkEmailDomain "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      removeDomainCatchAll "$arg"
      ;;
    u) # --add-user
      __r=""
      checkEmailAddress "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      addEmailUser "$arg"
      ;;
    U) # --remove-user
      __r=""
      checkEmailAddress "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      removeEmailUser "$arg"
      ;;
    a) # --add-alias
      __r=""
      checkEmailAlias "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      addEmailAlias "$arg"
      ;;
    A) # --remove-alias
      __r=""
      checkEmailAddress "$arg" "__r"
      optionIndex+=1
      if [[ -z "$__r" ]]; then
        exit 1
      fi
      removeEmailAlias "$arg"
      ;;
    l) # --list-all
      cat << EOF
#
# Domains
#
EOF
      listDomains

      cat << EOF

#
# Users
#
EOF
      listUsers

      cat << EOF

#
# Catch-Alls
#
EOF
      listCatchAlls

      cat << EOF

#
# Aliases
#
EOF
      listAliases
      ;;
    m) # --list-domains
      listDomains
      ;;
    n) # --list-users
      listUsers
      ;;
    o) # --list-catch-alls
      listCatchAlls
      ;;
    p) # --list-aliases
      listAliases
      ;;
    *)
      cliOptionError "$option" $optionIndex "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))


if [[ ${#doPostmap[*]} -ne 0 ]]; then
  for fl in "${!doPostmap[@]}"; do
    postmap "$fl"
  done
fi

if [[ $postfixActiveOnStart -ne 0 ]] && \
   [[ $doPostfixRestart -ne 0 ]]; then
  systemctl -q reload-or-restart "postfix.service"
fi

exit $exitCode
