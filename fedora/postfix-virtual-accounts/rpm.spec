%global vmailName       vmail
%global mailSpoolDir    %{_localstatedir}/spool/mail
%global vmailDir        %{mailSpoolDir}/vhosts
%global vmailDirBackup  %{mailSpoolDir}/vhosts.backup
%global vmailUidStart   50000
%global vmailGidStart   50000

%global postfixVhosts   %{_sysconfdir}/postfix/vhosts
%global postfixVirtual  %{_sysconfdir}/postfix/virtual
%global postfixVmailbox %{_sysconfdir}/postfix/vmailbox


Name:             postfix-virtual-accounts
Version:          1.0.1
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Enable postfix virtual accounts, with helper script

Requires:         bash-completion-pelagic-helpers
Requires:         postfix-as-mta

Requires:         bash
Requires:         coreutils
Requires:         gawk, grep
Requires:         glibc-common
Requires:         postfix
Requires:         shadow-utils
Requires:         sed, systemd


%description
Enable postfix virtual accounts, with helper script.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%pre

# install and updates

function createVmailAccount() {
  # create the group when needed
  if [[ -z "$(getent group "%{vmailName}" 2> /dev/null)" ]]; then
    local -i vmailNrGroup=%{vmailGidStart}
    local -i __nrOk=0
    while [[ $__nrOk -eq 0 ]]; do
      if [[ -z "$(getent group "$vmailNrGroup" 2> /dev/null)" ]]; then
        __nrOk=1
        groupadd \
          -g $vmailNrGroup \
          "%{vmailName}"
      else
        vmailNrGroup+=1
      fi
    done
  fi

  # create the user when needed
  if [[ -z "$(getent passwd "%{vmailName}" 2> /dev/null)" ]]; then
    local -i vmailNrUser=%{vmailUidStart}
    local -i __nrOk=0
    while [[ $__nrOk -eq 0 ]]; do
      if [[ -z "$(getent passwd "$vmailNrUser" 2> /dev/null)" ]]; then
        __nrOk=1
        useradd \
          -u $vmailNrUser \
          -g "%{vmailName}" \
          -c "Virtual Mail User" \
          -M \
          -s "/usr/sbin/nologin" \
          -d "%{mailSpoolDir}" \
          "%{vmailName}"
      else
        vmailNrUser+=1
      fi
    done
  fi
}

if [[ $1 -eq 1 ]]; then
  # install

  createVmailAccount
fi


%post

# install and updates

vmailNrGroup=$(getent group "%{vmailName}" 2> /dev/null | \
               awk -F ':' '{print $3}' 2> /dev/null)

vmailNrUser=$(getent passwd "%{vmailName}" 2> /dev/null | \
              awk -F ':' '{print $3}' 2> /dev/null)

declare -i vmailUidMin=$( \
  grep -iE '^[[:space:]]*UID_MIN[[:space:]]+[0-9]+[[:space:]]*$' "%{_sysconfdir}/login.defs" 2> /dev/null | \
  awk '{print $2}' 2> /dev/null)

postconf -e \
  "virtual_mailbox_base    = %{vmailDir}" \
  "virtual_mailbox_domains = %{postfixVhosts}" \
  "virtual_alias_maps      = hash:%{postfixVirtual}" \
  "virtual_mailbox_maps    = hash:%{postfixVmailbox}" \
  "virtual_uid_maps        = static:$vmailNrUser" \
  "virtual_gid_maps        = static:$vmailNrGroup" \
  "virtual_minimum_uid     = $vmailUidMin"

touch "%{postfixVirtual}"

postmap "%{postfixVirtual}"
postmap "%{postfixVmailbox}"

systemctl -q try-reload-or-restart "postfix.service"

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  /usr/sbin/postconf -e \
    "virtual_mailbox_base    = $(postconf -d -h "virtual_mailbox_base" 2> /dev/null)" \
    "virtual_mailbox_domains = $(postconf -d -h "virtual_mailbox_domains" 2> /dev/null)" \
    "virtual_alias_maps      = $(postconf -d -h "virtual_alias_maps" 2> /dev/null)" \
    "virtual_mailbox_maps    = $(postconf -d -h "virtual_mailbox_maps" 2> /dev/null)" \
    "virtual_uid_maps        = $(postconf -d -h "virtual_uid_maps" 2> /dev/null)" \
    "virtual_gid_maps        = $(postconf -d -h "virtual_gid_maps" 2> /dev/null)" \
    "virtual_minimum_uid     = $(postconf -d -h "virtual_minimum_uid" 2> /dev/null)"

  systemctl -q try-reload-or-restart "postfix.service"
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/postfix/*
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*
%dir %attr(-,vmail,vmail) %{vmailDir}
%dir %attr(-,vmail,vmail) %{vmailDirBackup}


%changelog
* Mon Apr 19 2021 Ferry Huberts - 1.0.1
- Improve the scripts
- Improve the spec file

* Fri Apr 09 2021 Ferry Huberts - 1.0.0
- Initial release
