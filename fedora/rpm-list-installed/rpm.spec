%global outputDir       /var/lib/rpm-list-installed
%global outputFile      %{outputDir}/rpm-list-installed.txt

Name:             rpm-list-installed
Version:          1.0.13
Release:          3%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Create a dialy list of all installed packages

Requires:         bash
Requires:         coreutils
Requires:         rpm
Requires:         systemd

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
Create a dialy list of all installed packages in the file
  %{outputFile}


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *

mkdir -p "%{buildroot}%{outputDir}"
touch "%{buildroot}%{outputFile}"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "%{name}.timer"
fi

if [[ $1 -eq 1 ]]; then
  # install

  systemctl -q enable            "%{name}.timer"
  systemctl -q reload-or-restart "%{name}.timer"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q stop    "%{name}.timer"
  systemctl -q disable "%{name}.timer"
fi

exit 0


%files
%defattr(-,root,root)
%{_sbindir}/*
%{_unitdir}/*
%ghost %{outputFile}


%changelog
* Thu Apr 29 2021 Ferry Huberts - 1.0.13
- Update the systemd unit files to not use the obsolete 'syslog' value

* Mon Apr 19 2021 Ferry Huberts - 1.0.12
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.0.11
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.10
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.9
- Bump to Fedora 27

* Tue Jun 13 2017 Ferry Huberts - 1.0.8
- Include only unique packages names in the list

* Tue Jun 13 2017 Ferry Huberts - 1.0.7
- Include the architecture in the packages names

* Mon Apr 17 2017 Ferry Huberts - 1.0.6
- Use bash' [[ builtin instead of the program [

* Fri Mar 03 2017 Ferry Huberts - 1.0.5
- Improve scriptlets

* Tue Dec 13 2016 Ferry Huberts - 1.0.4
- Fix rpmlint warnings

* Fri Dec 02 2016 Ferry Huberts - 1.0.3
- Bump to Fedora 25

* Mon Dec 21 2015 Ferry Huberts - 1.0.2
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.0.1
- only modify services when needed

* Mon Dec 14 2015 Ferry Huberts - 1.0.0
- Initial release
