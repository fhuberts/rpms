%global dovecotConfDir  %{_sysconfdir}/dovecot/conf.d

Name:             dovecot-on-postfix
Version:          1.0.18
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Configure dovecot to use postfix

Requires:         configfile-utils
Requires:         dovecot
Requires:         grep
Requires:         postfix-as-mta
Requires:         sed, systemd


%description
Configure dovecot to use postfix.


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

postconf -e 'smtpd_sasl_type = dovecot'

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/10-auth.conf" \
  "auth_mechanisms=plain login"

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/10-mail.conf" \
  "mail_location=maildir:~/"

socketGrep='[[:space:]]*unix_listener[[:space:]]+\/var\/spool\/postfix\/private\/auth[[:space:]]*\{[[:space:]]*'
dst="%{dovecotConfDir}/10-master.conf"
if [[ -z "$(grep -E "^$socketGrep$" "$dst")" ]]; then
  newSocket='\
  unix_listener \/var\/spool\/postfix\/private\/auth {\
    mode = 0660\
    user = postfix\
    group = postfix\
  }'
  sed -r -i -e "/^[[:space:]]*#+$socketGrep$/ i $newSocket" "$dst"
fi

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/20-imap.conf" \
  "imap_client_workarounds=delay-newmail tb-extra-mailbox-sep tb-lsub-flags"

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/20-lmtp.conf" \
  "lmtp_client_workarounds=whitespace-before-path mailbox-for-path"

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/20-pop3.conf" \
  "pop3_client_workarounds=outlook-no-nuls oe-ns-eoh"

configfile-adjust-settings \
  -s \
  -f "%{dovecotConfDir}/20-submission.conf" \
  "submission_client_workarounds=whitespace-before-path mailbox-for-path"

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart \
    "postfix.service" \
    "dovecot.service"
fi

if [[ $1 -eq 1 ]]; then
  # install

  for i in "" "--permanent"; do
    firewall-cmd -q $i \
      --add-service=imaps \
      --add-service=pop3s
  done

  systemctl -q enable                "dovecot.service"
  systemctl -q reload-or-restart     "dovecot.service"

  systemctl -q try-reload-or-restart "postfix.service"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  postconf -e "smtpd_sasl_type = $(postconf -d -h "smtpd_sasl_type")"

  for i in "" "--permanent"; do
    firewall-cmd -q $i \
      --remove-service=imaps \
      --remove-service=pop3s
  done

  systemctl -q stop                  "dovecot.service"
  systemctl -q disable               "dovecot.service"

  systemctl -q try-reload-or-restart "postfix.service"
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Sun Jun 27 2021 Ferry Huberts - 1.0.18
- Add client workarounds, seem to be needed

* Thu Apr 22 2021 Ferry Huberts - 1.0.17
- Finish moving enabling smtps and submission services to the postfix-as-mta
  package, the uninstall of smtpd_sasl_path was forgotten in the move.

* Wed Apr 21 2021 Ferry Huberts - 1.0.16
- Move enabling smtps and submission services to the postfix-as-mta package,
  as well as setting set smtpd_sasl_path.

* Sat Apr 17 2021 Ferry Huberts - 1.0.15
- Improve the spec file

* Sun Apr 11 2021 Ferry Huberts - 1.0.14
- Remove changing the certificate

* Mon Apr 06 2020 Ferry Huberts - 1.0.13
- Do not set disable_plaintext_auth, its default is the same

* Mon Apr 06 2020 Ferry Huberts - 1.0.12
- Fix the firewall command

* Sun Apr 05 2020 Ferry Huberts - 1.0.11
- Enable smtps and submission services too
- Open firewall ports on install
- Restart service after updates, when active
- Remove some unneeded configuration

* Wed Mar 03 2021 Ferry Huberts - 1.0.10
- Move dovecot specific postfix configuration here from postfix-as-mta rpm

* Sun Jul 05 2020 Ferry Huberts - 1.0.9
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.8
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.7
- Bump to Fedora 27

* Thu Mar 02 2017 Ferry Huberts - 1.0.6
- Improve %post scriptlet
- Remove %preun scriptlet

* Wed Dec 28 2016 Ferry Huberts - 1.0.5
- systemctl changes

* Tue Dec 27 2016 Ferry Huberts - 1.0.4
- Open imaps and pop3s ports

* Fri Dec 23 2016 Ferry Huberts - 1.0.3
- systemctl changes

* Mon Dec 05 2016 Ferry Huberts - 1.0.2
- Bump to Fedora 25

* Sat May 21 2016 Ferry Huberts - 1.0.1
- systemctl changes

* Tue May 10 2016 Ferry Huberts - 1.0.0
- Initial release
