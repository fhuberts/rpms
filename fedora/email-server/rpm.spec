Name:             email-server
Version:          1.0.2
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          An email server with support for virtual accounts

Requires:         postfix-as-mta
Requires:         dovecot-on-postfix

Requires:         postfix-virtual-accounts
Requires:         dovecot-virtual-accounts


%description
An email server with support for virtual accounts.

Uses postfix, dovecot.


%prep
%include ../spec-supported.include


%build


%install


%files


%changelog
* Thu May 26 2022 Ferry Huberts - 1.0.2
- Remove squirrelmail from dependencies; it doesn't work on Fedora 35 and
  is removed from Fedora 36.

* Sun Apr 18 2021 Ferry Huberts - 1.0.1
- Improve the spec file

* Sun Apr 11 2021 Ferry Huberts - 1.0.0
- Initial release
