#!/usr/bin/bash

set -e
set -u


targetBaseDirRel="../usr/share/vpn-server/manuals/$1"
shift 1



function texToPdf() {
  if [[ $# -lt 1 ]]; then
    echo "ERROR: Specify the tex doc to build a PDF from"
    return
  fi

  local texDoc="$1"

  if [[ ! -f "$texDoc" ]]; then
    echo "ERROR: tex doc '$texDoc' doesn't exist."
    return
  fi

  local texDocName="$(basename "$texDoc" 2> /dev/null)"
  local texDocDir="$(dirname "$texDoc" 2> /dev/null)"

  local texDocNamePDF="${texDocName%\.tex}.pdf"
  local texDocDirName="$(basename "$texDocDir" 2> /dev/null)"

  local dstPDF="$targetBaseDir/$texDocDirName/$texDocNamePDF"

  echo "###############################################################################"
  echo "# Processing document '$(basename "$texDoc" 2> /dev/null)'"
  echo "###############################################################################"

  if [[ $force -eq 0 ]]; then
    local -i texDocStat=$(stat --format=%Y "$texDoc" 2> /dev/null)

    local -i dstPDFStat=0
    if [[ -e "$dstPDF" ]]; then
      dstPDFStat=$(stat --format=%Y "$dstPDF" 2> /dev/null)
    fi

    if [[ $texDocStat -le $dstPDFStat ]]; then
      echo "# Document is up-to-date: skipped"
      return
    fi
  fi

  local texDocDirTmp="$texDocDir/${texDocName%\.tex}.pdflatex"
  mkdir -p "$texDocDirTmp"

  pushd "$texDocDir" &> /dev/null
    for i in 1 2; do
      pdflatex -output-directory "$texDocDirTmp" "$texDocName" 2>&1
    done
  popd &> /dev/null

  mkdir -p "$(dirname "$dstPDF" 2> /dev/null)"
  gs \
    -sDEVICE=pdfwrite \
    -dCompatibilityLevel=2.0 \
    -dNOPAUSE \
    -dQUIET \
    -dBATCH \
    -sOutputFile="$dstPDF" \
    "$texDocDirTmp/$texDocNamePDF"
}




startDir="$(pwd 2> /dev/null)"
pushd "$startDir" &> /dev/null
startDir="$(pwd 2> /dev/null)"
popd &> /dev/null

scriptDir="$(dirname "$0" 2> /dev/null)"
pushd "$scriptDir" &> /dev/null
scriptDir="$(pwd 2> /dev/null)"
popd &> /dev/null

targetBaseDir="$startDir/$targetBaseDirRel"
if [[ ! -d "$targetBaseDir" ]]; then
  mkdir -p "$targetBaseDir"
fi
pushd "$targetBaseDir" &> /dev/null
targetBaseDir="$(pwd 2> /dev/null)"
popd &> /dev/null


force=0
if [[ $# -ne 0 ]] && [[ "$1" == "--force" ]]; then
  force=1
  shift 1
fi


subdirs=()
while [[ $# -ne 0 ]]; do
  if [[ ! -d "$1" ]]; then
    echo "ERROR: directory '$1' doesn't exist'"
    exit 1
  fi
  pushd "$1" &> /dev/null
  subdirs+=( "$(pwd 2> /dev/null)" )
  popd &> /dev/null
  shift 1
done


if [[ ! -d "$scriptDir" ]]; then
  echo "ERROR: script directory '$scriptDir' doesn't exist."
  exit 1
fi

if [[ ! -d "$targetBaseDir" ]]; then
  echo "ERROR: target base directory '$targetBaseDir' doesn't exist."
  exit 1
fi

if [[ ${#subdirs[*]} -eq 0 ]]; then
  ifsOld="$IFS"
  pushd "$startDir" &> /dev/null
  IFS=$'\n'
  subdirs=( $(find * -mindepth 1 -maxdepth 1 -type f -name '*.tex' -exec dirname '{}' \; 2> /dev/null | \
              sort -u 2> /dev/null) )
  IFS="$ifsOld"
  popd &> /dev/null
fi


for subdir in "${subdirs[@]}"; do
  pushd "$startDir" &> /dev/null
  if [[ ! -d "$subdir" ]]; then
    echo "ERROR: sub-directory '$subdir' doesn't exist."
    exit 1
  fi
  popd &> /dev/null
done

for subdir in "${subdirs[@]}"; do
  pushd "$startDir" &> /dev/null
  ifsOld="$IFS"
  IFS=$'\n'
  texs=( $(find "$startDir/$subdir" -type f -name '*.tex' 2> /dev/null | \
           sort 2> /dev/null) )
  IFS="$ifsOld"

  if [[ ${#texs[*]} -eq 0 ]]; then
    continue
  fi

  for i in "${texs[@]}"; do
    texToPdf "$i"
  done
  popd &> /dev/null
done

exit 0
