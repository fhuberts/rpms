#!/usr/bin/bash

set -e
set -u


declare script="$0"
declare scriptName="$(basename "$script" 2> /dev/null)"


if [[ $# -ne 1 ]]; then
  echo "ERROR: specify the configuration name."
  exit 1
fi

cfgName="$1"
cfgFile="/etc/ssh-rsync-backup/$cfgName.conf"

if [[ ! -r "$cfgFile" ]]; then
  echo "ERROR: configuration $cfgName not found."
  exit 1
fi

source "$cfgFile"


declare lockDir="/var/lock/subsys"
declare lockFile="$lockDir/$scriptName"


# ============================================================================


function removelogfile() {
  local module="$1"
  local logfile="$2"

  if [[ -z "$logfile" ]]; then
    return
  fi

  if [[ -s "$logfile" ]]; then
    ERRORS=1
    cat << EOF

################################################################################
# $module
################################################################################
EOF
    cat "$logfile"
  fi

  rm -f "$logfile"
}


# output non-empty log files (but do not remove them), remove empty log files
function removealllogfiles() {
  local -i index=0
  while [[ $index -lt ${#moduleNames[*]} ]]; do
    removelogfile "${moduleNames[$index]}" "${logfiles[$index]}"
    logfiles[$index]=""
    index+=1
  done
}


#
# Exit Handler
#
trap exittrapbackup EXIT

MAINLOG=""
function exittrapbackup() {
  local -i exitCode=$?
  if [[ -n "$MAINLOG" ]] && [[ -f "$MAINLOG" ]]; then
    rm -f "$MAINLOG"
    MAINLOG=""
  fi
  if [[ $exitCode -ne 0 ]]; then
    local mod="${module:-}"
    if [[ -n "$mod" ]]; then
      echo "ERROR: exit with error code $exitCode while processing '$mod'"
    fi
  fi
  removealllogfiles
  exit $exitCode
}


function getduration() {
  local -i dur=$(( $2 - $1 ))
  date -u -d @$dur +"%T"
}


#
# Filter modules by regex
#
# 1      = the regex
# 2      = 0=get non-matching 1=get matching
# 3...   = entries to filter
# output = requested entries, each one on a new line
function filterModulesByRegex() {
  local regex="$1"
  local -i matching=$2
  shift 2

  while [[ $# -gt 0 ]]; do
    local matches="$(echo "$1" | grep -Ei "$regex" 2> /dev/null)"
    if [[ -z "$matches" ]] && [[ $matching -eq 0 ]]; then
      echo "$1"
    elif [[ -n "$matches" ]] && [[ $matching -eq 1 ]]; then
      echo "$1"
    fi
    shift 1
  done
}


#
# Run the entire backup procedure
#
function doit() {
  local ifsold="$IFS"
  local -a modules=()
  if [[ $useBackupPaths -ne 0 ]]; then
    modules=( "${backupPaths[@]}" )
  else
    # get all rsync modules
    IFS=$'\n'
    modules=( $(rsync -e "ssh -p $serverPort" "$userName@$serverName::"  2> /dev/null| \
                sed -r \
                    -e 's/^[[:space:]]+//' \
                    -e 's/[[:space:]]+$//' \
                    2> /dev/null \
                    | \
                sort ) )
    IFS="$ifsold"
  fi

  local -i modulesCount=${#modules[*]}
  local -i totalCount=0

  # exit when there are no modules to backup
  if [[ $modulesCount -eq 0 ]]; then
    exit 0
  fi

  # get the maximum string length of all modules
  local -i maxlen=0
  local module=""
  for module in "${modules[@]}"; do
    local -i modlen=${#module}
    if [[ $modlen -gt $maxlen ]]; then
      maxlen=$modlen
    fi
  done # for module in "${modules[@]}"; do

  if [[ ! -d "$backupDir" ]]; then
    echo "ERROR: backup directory '$backupDir' does not exist."
    exit 1
  fi # if [[ ! -d "$backupDir" ]]; then

  pushd "$backupDir" &> /dev/null

  for backupPhaseRegex in "${backupPhaseRegexes[@]}"; do

    echo "STARTED  at $(date "+%R" 2> /dev/null)"

    local timestamp="$(date "+%Y%m%d%H%M%S" 2> /dev/null)"

    IFS=$'\n'
    local -a modulesThisPhase=( $(filterModulesByRegex "$backupPhaseRegex" 1 "${modules[@]}") )
    modules=( $(filterModulesByRegex "$backupPhaseRegex" 0 "${modules[@]}") )
    IFS="$ifsold"

    moduleNames=()
    logfiles=()
    starttimes=()
    jobids=()

    # start backup jobs for all modules
    for module in "${modulesThisPhase[@]}"; do

      # a --archive		archive mode; equals -rlptgoD (no -H,-A,-X)
      #   r --recursive	recurse into directories
      #   l --links		copy symlinks as symlinks
      #   p --perms		preserve permissions
      #   t --times		preserve modification times
      #   g --group		preserve group
      #   o --owner		preserve owner (super-user only)
      #   D			same as --devices --specials
      #     --devices	preserve device files (super-user only)
      #     --specials	preserve special files

      # i --itemize-changes	output a change-summary for all updates
      # h --human-readable	output numbers in a human-readable format
      # H --hard-links	preserve hard links
      # S --sparse		handle sparse files efficiently
      # R --relative	use relative path names

      local rsyncModuleBase="$(basename "$module" 2> /dev/null)"

      local rsyncModule="$module"
      local rsyncModuleDst="."
      local -i sleepTime=1
      if [[ $useBackupPaths -eq 0 ]]; then
        rsyncModule=":$module"
        rsyncModuleDst="$rsyncModuleBase"
      fi

      local logfile="$(mktemp "$timestamp.$scriptName.$rsyncModuleBase.XXXXXXXXXX.log" 2> /dev/null)"

      moduleNames+=( "$module" )
      logfiles+=( "$logfile" )
      starttimes+=( "$(date "+%s" 2> /dev/null)" )

      if [[ $parallelRun -ne 0 ]]; then
        rsync -e "ssh -p $serverPort" -aihHS$verbose$check $progress --delete --ignore-errors "$userName@$serverName:$rsyncModule" "$rsyncModuleDst" 2>&1 > "$logfile" &
        jobids+=( $! )
        # avoid overloading the remote end
        sleep $sleepTime
      else # if [[ $parallelRun -ne 0 ]]; then
        rsync -e "ssh -p $serverPort" -aihHS$verbose$check $progress --delete --ignore-errors "$userName@$serverName:$rsyncModule" "$rsyncModuleDst" 2>&1 | tee > "$logfile"
        jobids+=( 0 )
      fi # if [[ $parallelRun -ne 0 ]]; then
    done # for module in "${modulesThisPhase[@]}"; do

    # exit when no backup jobs were started
    if [[ $parallelRun -ne 0 ]] \
       && [[ ${#jobids[*]} -eq 0 ]]; then
      exit 0
    fi

    # wait for all jobs to finish
    local -i jobsleft=$parallelRun
    while [[ $jobsleft -gt 0 ]]; do
      jobsleft=0

      local -i index=0
      while [[ $index -lt ${#jobids[*]} ]]; do
        local -i jobid=${jobids[$index]}

        if [[ $jobid -ne 0 ]]; then
          set +e
          local jobps="$(ps -q "$jobid" -o pid= 2> /dev/null)"
          set -e

          if [[ -z "$jobps" ]]; then
            printf "%${maxlen}s : %s\n" "${moduleNames[$index]}" "$(getduration ${starttimes[$index]} $(date "+%s" 2> /dev/null))"
            jobids[$index]=0

            removelogfile "${moduleNames[$index]}" "${logfiles[$index]}"
            logfiles[$index]=""
            starttimes[$index]=""
          else # if [[ -z "$jobps" ]]; then
            jobsleft+=1
          fi # if [[ -z "$jobps" ]]; then
        fi # if [[ $jobid -ne 0 ]]; then

        index+=1
      done # while [[ $index -lt ${#jobids[*]} ]]; do

      if [[ $jobsleft -gt 0 ]]; then
        sleep 10
      fi
    done # while [[ $jobsleft -gt 0 ]]; do

    totalCount+=${#modulesThisPhase[*]}

    removealllogfiles

    echo "FINISHED at $(date "+%R" 2> /dev/null)"

  done # for backupPhaseRegex in "${backupPhaseRegexes[@]}"; do

  popd &> /dev/null

  if [[ $modulesCount -ne $totalCount ]]; then
    ERRORS=1
    cat << EOF

ERROR: Something went wrong. The expected number of backups were not made.
         modulesCount = $modulesCount
         totalCount   = $totalCount
EOF
    exit 1
  fi # if [[ $modulesCount -ne $totalCount ]]; then
}


#
# Called when the program is interrupted
#
function interruptTrapHandler() {
  exit 126
}




# ============================================================================

declare -i ERRORS=0
declare -a moduleNames=()
declare -a logfiles=()
declare -a starttimes=()
declare -a jobids=()


# Add a catch-all for the backup phases
backupPhaseRegexes+=( '.*' )


#
# Run the main handler in a lock: the script can't run in parallel
#

# create lockfile
if [[ ! -d "$lockDir" ]]; then
  mkdir -p "$lockDir"
fi
if [[ ! -e "$lockFile" ]]; then
  touch "$lockFile"
  chmod 600 "$lockFile"
fi

# probe lockfile
set +e
( flock -e -n 200 ) 200> "$lockFile"
declare -i lockResult=$?
set -e
if [[ $lockResult -ne 0 ]]; then
  echo "ERROR: another invocation is already running."
  exit 1
fi

# clear exit handlers to avoid double stack trace
trap - EXIT
trap - SIGINT
trap - SIGHUP

(
  # setup the exit handlers
  trap exittrapbackup EXIT
  trap interruptTrapHandler SIGINT
  trap interruptTrapHandler SIGHUP

  # acquire the lock
  set +e
  flock -e -w 0 200
  declare -i lockResult=$?
  set -e

  if [[ $lockResult -ne 0 ]]; then
    echo "ERROR: another invocation is already running."
    exit 1
  fi

  MAINLOG="$(mktemp 2> /dev/null)"

  # run the backup
  doit &> "$MAINLOG"

  if [[ $ERRORS -ne 0 ]] || [[ $alwaysSendMail -ne 0 ]]; then
    msg="Success"
    if [[ $ERRORS -ne 0 ]]; then
      msg="Failure"
    fi

    cat "$MAINLOG" | \
    mail \
      -S sendwait \
      -s "$scriptName: $msg" \
      "$mailTo"
  fi

  rm -f "$MAINLOG"
  MAINLOG=""

  exit 0
) 200>"$lockFile"

# do not restore exit handler, we're at the end already

# end of the program
