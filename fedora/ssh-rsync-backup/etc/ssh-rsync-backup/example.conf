# Configuration for using rsync over ssh to backup one machine on another.
#
# It supports using a list of paths to backup or backing up all modules from a
# remote rsync daemon.

# The remote server.
serverName="server.example.com"

# The remove SSH port.
serverPort="22"

# The remote user.
userName="root"

# The LOCAL (base) directory for backups.
backupDir="/mnt/backups/$serverName.backup"

# Set to 1 to use the specified list of (remote) backup paths.
# Set to 0 to backup all modules on the remote rsync daemon.
declare -i useBackupPaths=1

# The list of (remote) backup paths. Only relevant when 'useBackupPaths' is set
# to 1.
declare -a backupPaths=(
  "/mnt/data/doc"
  "/mnt/data/src"
)

# Set to 1 to backup all paths or modules in parallel.
declare -i parallelRun=1

# When there are (too) many paths or modules to backup then the list of
# extended regular expressions below can be used to divide the backup in
# phases. Each phase will backup all paths or modules that match the current
# extended regular expression (case-insensitive). Subsequent phases will never
# match paths or modules that were already backed up in a previous phase. The
# extended regular expression '.*' is automatically appended to the end of
# this list to serve as a catch-all to ensure all paths or modules are backed
# up.
declare -a backupPhaseRegexes=(
)


# Keep empty to trust file size and modification time.
# Set to 'c' to check integrity with checksums.
check=''

# Set to 'q' to be quiet.
# Set to 'v' to be verbose.
verbose='q'

# Keep empty to not show file transfer progress.
# Set to '--progress' to show file transfer progress.
progress=''


# Set to 1 to always send a report by email.
# Set to 0 to only send a report when there were errors.
declare -i alwaysSendMail=0

# The email address to send email to.
mailTo="admin@example.com"
