Name:             ssh-rsync-backup
Version:          1.0.2
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          rsync-over-ssh daily backup

Requires:         bash
Requires:         coreutils
Requires:         grep
Requires:         mailx
Requires:         openssh-clients
Requires:         rsync
Requires:         sed, systemd

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
rsync-over-ssh daily backup.

Create a new configuration 'server' by copying the file
  /etc/ssh-rsync-backup/example.conf
to the file
  /etc/ssh-rsync-backup/server.conf
and adjusting it.

Then enable the backup timer by running
  systemctl -q enable \
    "ssh-rsync-backup@server.service" \
    "ssh-rsync-backup@server.timer"
  systemctl -q reload-or-restart \
    "ssh-rsync-backup@server.timer"


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post


%files
%defattr(-,root,root)
%{_sysconfdir}/%{name}/*
%{_unitdir}/*
%{_sbindir}/*


%changelog
* Tue May 03 2022 Ferry Huberts - 1.0.2
- Fix the systemd service file

* Mon Apr 19 2021 Ferry Huberts - 1.0.1
- Improve the script
- Improve the spec file

* Sun Feb 28 2021 Ferry Huberts - 1.0.0
- Initial release
