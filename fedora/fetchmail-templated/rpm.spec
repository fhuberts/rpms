Name:             fetchmail-templated
Version:          1.0.3
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          fetchmail, in templated service form

Requires:         fetchmail
Requires:         systemd

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
Fetchmail, in templated service form.
Allows usage of IMAP idle for multiple accounts.

Be careful not to have multiple service instances fetch from the same
account.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -pv "%{buildroot}"
cp -av -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  semanage fcontext -a -t fetchmail_var_run_t '%{_sysconfdir}/fetchmail.d(/.*)?'
  restorecon -R %{_sysconfdir}/fetchmail.d
fi

exit 0


%postun

if [[ $1 -eq 0 ]]; then
  # uninstall

  semanage fcontext -d -t fetchmail_var_run_t '%{_sysconfdir}/fetchmail.d(/.*)?'
fi

exit 0


%files
%defattr(-,root,root)
%dir %attr(-,mail,root) %{_sysconfdir}/fetchmail.d/joe@example.com/
%attr(600,mail,root) %{_sysconfdir}/fetchmail.d/joe@example.com/*
%{_unitdir}/*


%changelog
* Wed Feb 23 2022 Ferry Huberts - 1.0.3
Always restart the service

* Sat Dec 11 2021 Ferry Huberts - 1.0.2
Always restart the service

* Sun Apr 18 2021 Ferry Huberts - 1.0.1
- Improve the spec file

* Wed Apr 14 2021 Ferry Huberts - 1.0.0
- Initial release

