%global grubDefault         %{_sysconfdir}/default/grub
%global grubBootCfg         /boot/grub2/grub.cfg
%global grubBootEfiCfg      /boot/efi/EFI/fedora/grub.cfg
%global GRUBTIMEOUT         0
%global GRUBTIMEOUTRESTORE  3

Name:             grub-timeout-zero
Version:          1.0.6
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Set the GRUB boot timeout to zero

Requires:         configfile-utils

Requires:         sed


%description
Set the GRUB boot timeout to zero


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  if [[ -f "%{grubDefault}" ]]; then
    configfile-adjust-settings \
      -b \
      -f "%{grubDefault}" \
      "GRUB_TIMEOUT=%{GRUBTIMEOUT}"
  fi

  if [[ -f "%{grubBootCfg}" ]]; then
    sed -r -i \
      "s/^([[:space:]]*set[[:space:]]*timeout[[:space:]]*=[[:space:]]*)[[:digit:]]+[[:space:]]*\$/\1%{GRUBTIMEOUT}/" \
      "%{grubBootCfg}"
  fi

  if [[ -f "%{grubBootEfiCfg}" ]]; then
    sed -r -i \
      "s/^([[:space:]]*set[[:space:]]*timeout[[:space:]]*=[[:space:]]*)[[:digit:]]+[[:space:]]*\$/\1%{GRUBTIMEOUT}/" \
      "%{grubBootEfiCfg}"
  fi
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  if [[ -f "%{grubDefault}" ]]; then
    configfile-adjust-settings \
      -b \
      -f "%{grubDefault}" \
      "GRUB_TIMEOUT=%{GRUBTIMEOUTRESTORE}"
  fi

  if [[ -f "%{grubBootCfg}" ]]; then
    sed -r -i \
      "s/^([[:space:]]*set[[:space:]]*timeout[[:space:]]*=[[:space:]]*)[[:digit:]]+[[:space:]]*\$/\1%{GRUBTIMEOUTRESTORE}/" \
      "%{grubBootCfg}"
  fi

  if [[ -f "%{grubBootEfiCfg}" ]]; then
    sed -r -i \
      "s/^([[:space:]]*set[[:space:]]*timeout[[:space:]]*=[[:space:]]*)[[:digit:]]+[[:space:]]*\$/\1%{GRUBTIMEOUTRESTORE}/" \
      "%{grubBootEfiCfg}"
  fi
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Sun Apr 18 2021 Ferry Huberts - 1.0.6
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.0.5
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.4
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.3
- Bump to Fedora 27

* Thu Mar 02 2017 Ferry Huberts - 1.0.2
- Improve %post scriptlet

* Fri Dec 02 2016 Ferry Huberts - 1.0.1
- Bump to Fedora 25
- Do not depend on grub, just adjust when the relevant files can be found

* Thu Apr 28 2016 Ferry Huberts - 1.0.0
- Initial release, split off of base-server 1.6.7
