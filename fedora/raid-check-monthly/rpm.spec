Name:             raid-check-monthly
Version:          1.0.1
Release:          4%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Monthly RAID setup health check

Requires:         mdadm
Requires:         systemd

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros


%description
Monthly RAID setup health check.

Note: Disables the weekly raid-check that is setup by mdadm.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "%{name}.timer"
fi

if [[ $1 -eq 1 ]]; then
  # install

  systemctl -q stop \
    "raid-check.timer" \
    "raid-check.service"

  systemctl -q disable \
    "raid-check.timer" \
    "raid-check.service"

  systemctl -q enable            "%{name}.timer"
  systemctl -q reload-or-restart "%{name}.timer"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q stop              "%{name}.timer"
  systemctl -q disable           "%{name}.timer"

  systemctl -q enable \
    "raid-check.service" \
    "raid-check.timer"

  systemctl -q reload-or-restart "raid-check.timer"
fi

exit 0


%files
%defattr(-,root,root)
%{_unitdir}/*


%changelog
* Mon Apr 19 2021 Ferry Huberts - 1.0.1
- Improve the spec file

* Mon May 11 2020 Ferry Huberts - 1.0.0
- Initial release

