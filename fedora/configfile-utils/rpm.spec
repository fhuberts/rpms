Name:             configfile-utils
Version:          1.4.4
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Utilities for configuration files

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils
Requires:         grep
Requires:         sed


%description
Utilities for configuration files.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*


%changelog
* Sat Apr 17 2021 Ferry Huberts - 1.4.4
- Improve the scripts
- Improve the spec file

* Wed Feb 24 2021 Ferry Huberts - 1.4.3
- Move some bash functions to an include file

* Sun Feb 21 2021 Ferry Huberts - 1.4.2
- Rework and simplify bash completion

* Sun Feb 21 2021 Ferry Huberts - 1.4.1
- Small fixes to CLI parsing

* Sat Feb 20 2021 Ferry Huberts - 1.4.0
- Improve usage and add bash completion

* Sun Jul 05 2020 Ferry Huberts - 1.3.6
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.3.5
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.3.4
- Bump to Fedora 27

* Mon Apr 17 2017 Ferry Huberts - 1.3.3
- Use bash' [[ builtin instead of the program [

* Wed Dec 21 2016 Ferry Huberts - 1.3.2
- Add -D option to be able to remove settings

* Fri Dec 02 2016 Ferry Huberts - 1.3.1
- Bump to Fedora 25

* Mon Apr 25 2016 Ferry Huberts - 1.3.0
- add php variable format

* Thu Dec 31 2015 Ferry Huberts - 1.2.0
- add mail aliases format (-m)

* Wed Dec 23 2015 Ferry Huberts - 1.1.0
- add do-not-overwrite mode

* Wed Dec 16 2015 Ferry Huberts - 1.0.1
- add 2 new formats 'name = value' and 'name = "value"'

* Tue Dec 15 2015 Ferry Huberts - 1.0.0
- Initial release
