# VPN Server

This project produces RPMs for a VPN server appliance on Fedora.

The RPMs are meant to be deployed on top of a minimum Fedora installation,
but should work just as well on differently installed machines.


# Adjust The Configuration

The configuration file ```/etc/vpn-server/vpn-server.conf``` must be
adjusted such that it contains the desired settings.


# Setup The Server

```
vpn-server-setup --setup
```

Also see its man page; ```man vpn-server-setup```.


# Uninstall

The server can be uninstalled by:

```
vpn-server-setup --remove
```

Also see its man page; ```man vpn-server-setup```.


# Migration

The ```vpn-server-migrate``` application can be used to migrate the OpenVPN CA
directory from a previous server to a new one. See its man page for more
information.


# Managing The VPN Server

Please read the document
```/usr/share/vpn-server/manuals/server/Server Management/VPN Server Management.en_US.pdf```
after installing the RPM ```vpn-server-en-US```.

Translated versions might be available depending on which RPMs are installed.
