%global configFile    %{_sysconfdir}/%{name}/%{name}.conf
%global siteName      default

Name:             vpn-server
Version:          2.11.2
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          VPN Server applications and common files

Requires:         bash, bash-completion-pelagic-helpers, bridge-utils
Requires:         coreutils
Requires:         dos2unix
Requires:         easy-rsa
Requires:         findutils, firewalld
Requires:         gawk, grep
Requires:         ipcalc, iproute
Requires:         mailx
Requires:         NetworkManager
Requires:         openssl
Requires:         openvpn >= 2.4.1
Requires:         policycoreutils-python-utils, procps-ng
Requires:         sed, systemd
Requires:         zip

BuildRequires:    systemd
BuildRequires:    systemd-rpm-macros

Obsoletes:        %{name}-en-US < 1.1
Obsoletes:        %{name}-nl-NL < 1.1

Obsoletes:        %{name}-doc-client-fedora-25-en-US <= %{version}
Obsoletes:        %{name}-doc-client-fedora-33-en-US <= %{version}
Obsoletes:        %{name}-doc-client-fedora-37-en-US <= %{version}
Obsoletes:        %{name}-doc-client-windows-7-en-US <= %{version}
Obsoletes:        %{name}-doc-client-windows-8.1-en-US <= %{version}
Obsoletes:        %{name}-doc-client-windows-xp-en-US <= %{version}

Obsoletes:        %{name}-doc-client-fedora-25-nl-NL <= %{version}
Obsoletes:        %{name}-doc-client-fedora-33-nl-NL <= %{version}
Obsoletes:        %{name}-doc-client-fedora-37-nl-NL <= %{version}
Obsoletes:        %{name}-doc-client-windows-7-nl-NL <= %{version}
Obsoletes:        %{name}-doc-client-windows-8.1-nl-NL <= %{version}
Obsoletes:        %{name}-doc-client-windows-xp-nl-NL <= %{version}

%description
Applications to setup and operate a VPN server that offers VPN services for the
following protocol-topology combinations:
- UDP - routed
- UDP - bridged
- TCP - routed
- TCP - bridged

The UDP options are preferred over the TCP options since they deliver much
more performance. The reason for that is that the UDP options allow the network
stacks on the client and server systems to perform proper window scaling and
such.

The routed options are preferred over the bridged options since the routed
options operate on the IP protocol level of the network stack and therefore use
less bandwidth (as opposed to the bridged options that operate on the Ethernet
level of the network stack).

The package includes a default configuration that needs to be adjusted before
setting up the VPN server with the 'vpn-server-setup' application.


%package          nl-NL
Summary:          %{name} Dutch files

Requires:         %{name}


%description      nl-NL
The Dutch documentation package for %{name}.

Contains:
- email template
- man pages


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build
find ".%{_mandir}" -type f -exec gzip '{}' \;


%install
mkdir -pv "%{buildroot}"
cp -av -t "%{buildroot}" *

mv -v "%{buildroot}%{configFile}" "%{buildroot}/%{configFile}.%{siteName}"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart \
    "vpn-server-certificate-expiration-checks.timer" \
    "vpn-server-crl-refresh.timer"
fi

if [[ $1 -eq 1 ]]; then
  # install

  if [[ ! -f "%{configFile}" ]]; then
    cp -f "%{configFile}.%{siteName}" "%{configFile}"
  fi

  systemctl -q enable \
    "vpn-server-certificate-expiration-checks.timer" \
    "vpn-server-crl-refresh.timer"

  systemctl -q reload-or-restart \
    "vpn-server-certificate-expiration-checks.timer" \
    "vpn-server-crl-refresh.timer"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  systemctl -q stop \
    "vpn-server-certificate-expiration-checks.timer" \
    "vpn-server-crl-refresh.timer"

  systemctl -q disable \
    "vpn-server-certificate-expiration-checks.timer" \
    "vpn-server-crl-refresh.timer"
fi

exit 0


%files
%defattr(-,root,root)
%config %{_sysconfdir}/openvpn/server/*
%config %{_sysconfdir}/sysctl.d/*
%config %{_sysconfdir}/vpn-server/*
%{_sbindir}/*
%{_unitdir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/functions.include
%{_datarootdir}/%{name}/openvpn/*
%{_datarootdir}/%{name}/vpn-server.conf
%lang(en) %{_datarootdir}/%{name}/*/*.en_US.*
%lang(en) %{_mandir}/*/*.gz


%files nl-NL
%lang(nl) %{_datarootdir}/%{name}/*/*.nl_NL.*
%lang(nl) %{_mandir}/nl/*/*.gz


%changelog
* Mon May 01 2023 Ferry Huberts - 2.11.2
- Add verbose mode to the certificate expiration checks man pages
- Move the English man pages, otherwise they don't work

* Sat Apr 29 2023 Ferry Huberts - 2.11.1
- Add 'float' to the client configurations
- Add verbose mode to certificate expiration checks

* Sat Apr 08 2023 Ferry Huberts - 2.11.0
- Move to self-contained ovpn files for clients

* Thu Apr 06 2023 Ferry Huberts - 2.10.0
- Remove compression on server side and client side

* Sat Mar 25 2023 Ferry Huberts - 2.9.0
- Adjust to easy-rsa changes

* Fri Feb 10 2023 Ferry Huberts - 2.8.0
- No longer support Windows XP/7/8.1 and Fedora 25

* Thu Apr 29 2021 Ferry Huberts - 2.7.6
- Update the systemd unit files to not use the obsolete 'syslog' value

* Tue Apr 20 2021 Ferry Huberts - 2.7.5
-- Improve the scripts
- Improve the spec file

* Sun Feb 21 2021 Ferry Huberts - 2.7.4
- Rework and simplify bash completion

* Sun Feb 21 2021 Ferry Huberts - 2.7.3
- Small fixes to CLI parsing

* Sat Feb 20 2021 Ferry Huberts - 2.7.2
- Improve completion of vpn-server-setup

* Sat Feb 20 2021 Ferry Huberts - 2.7.1
- Small fixes to CLI parsing

* Fri Feb 19 2021 Ferry Huberts - 2.7.0
- Add --passwordfile options to renew-client and send

* Thu Feb 18 2021 Ferry Huberts - 2.6.0
- Make it possible to have a static IP configuration

* Sun Feb 14 2021 Ferry Huberts - 2.5.0
- Move manuals into a subdirectory
- Fix completion of manuals
- Just use the filename for the selected manual
- Perform validations as soon as possible
- Filter the options that are already on the CLI from the completions

* Sat Feb 13 2021 Ferry Huberts - 2.4.0
- Talk about applications instead of scripts
- Fix bash lower-casing syntax
- Small fix
- Add CLI option parsing and usage messages
- Setup now uses CLI options
- Make sure to clear outputs before running rest of functions
- Small fix
- Trim some function arguments
- The certificate name is always lowercase
- Simplify checkCertificateName
- Simplify checkEmailAddress
- Add checkLanguage function
- Also build up RUN_CLIENT_KEYS_MAP in getClientKeys
- Certificate-build now uses CLI options
- Certificate-change now uses CLI options
- Certificate-list now uses CLI options
- Certificate-renew-client now uses CLI options
- Certificate-revoke now uses CLI options
- Sort supported platforms
- Slight improvement of topology and protocol selection
- Certificate-send now uses CLI options
- Add bash completions
- Use a function to trim strings

* Sat Feb 06 2021 Ferry Huberts - 2.3.0
- Rename some applications

* Sat Feb 06 2021 Ferry Huberts - 2.2.0
- Fix a typo
- Fix the security warning when renewing the CA certificate
- Make choices before performing the CA renewal
- Improve language choice
- Move an error report
- Store & use language preference per certificate

* Mon Feb 01 2021 Ferry Huberts - 2.1.0
- More efficient printing of messages
- Fix the 'refresh CRL' timer description
- Use a better language choice check
- List the keys in a nicer layout
- Add expiration checks and renewal applications
- Talk about certificates, avoid talking about keys

* Wed Jan 27 2021 Ferry Huberts - 2.0.3
- make the default language configurable
- fix a comment
- do not error out in refresh crl when server is not setup
- do require an email template to be used
- fix a sorting issue when getting client keys

* Mon Jan 25 2021 Ferry Huberts - 2.0.2
- Rework email templates setup.

* Sun Jan 17 2021 Ferry Huberts - 2.0.1
- Android documentation was added, support it.

* Sat Jan 16 2021 Ferry Huberts - 2.0.0
- Switch to using easy-rsa 3, as provided by the distro
- Store and use email addresses belonging to keys

* Sat Jan 16 2021 Ferry Huberts - 1.1.18
- Use a configured regex for illegal key names
- Check that a reserved name is not used when building a key
- Add and use function getNmConnectionName
- Make setupNetworkBridge more robust
- Remove adjustBridgeScript, not needed
- Small improvement in checkCADirectoryIsPresent
- Improve CA and CRL copy functions
- Small fixups
- Fix shebangs
- Small improvements to zip-and-send
- Neater generated files in zip-and-send
- Make setup and remove more robust and correct
- Let the VPN be the default gateway
- Add configuration variables for organisation and email
- Replace use of KEY_ORG by CFG_ORGANISATION
- Replace use of KEY_EMAIL by CFG_ORGANISATION_EMAIL
- Update bridge and ethernet interface names
- Do not masquerade by default
- When no client keys were found report this
- Always ask for the key, even when there is only 1 key

* Sat Jan 16 2021 Ferry Huberts - 1.1.17
- Fix the firewalling setup, broken since Fedora 32
  switched to nftables

* Tue Apr 28 2020 Ferry Huberts - 1.1.16
- Make it build on Fedora 32

* Fri Aug 30 2019 Ferry Huberts - 1.1.15
- When sending mail wait for completion

* Wed Jul 31 2019 Ferry Huberts - 1.1.14
- Use quiet mode for firewall-cmd

* Thu Dec 27 2018 Ferry Huberts - 1.1.13
- Rename revoked keys and put them into the 'revoked' subdirectory under
  the directory with active keys.

* Wed Jun 06 2018 Ferry Huberts - 1.1.12
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.1.11
- Bump to Fedora 27

* Thu May 18 2017 Ferry Huberts - 1.1.10
- Use /run directly instead of /var/run

* Thu May 18 2017 Ferry Huberts - 1.1.9
- CA and config files are now located in /etc/openvpn/server/
- Use the new systemd setup

* Thu May 18 2017 Ferry Huberts - 1.1.8
- Fix the spec file w.r.t. installed files

* Thu May 18 2017 Ferry Huberts - 1.1.7
- Fix the bridge-start script
- Minor improvement to spec file changelog
- Update the README
- Get rid of net-tools usage
- Move the bridge-start script into /etc/openvpn/server

* Thu May 18 2017 Ferry Huberts - 1.1.6
- Only create the configuration file on install
- Adjust the CRL validity time during setup
- Refresh the crl every year
- Make the bridge-start script a bit more generic and robust

* Mon Apr 17 2017 Ferry Huberts - 1.1.5
- Use bash' [[ builtin instead of the program [

* Mon Apr 10 2017 Ferry Huberts - 1.1.4
- Adjust to the renamed openvpn /run/ directory

* Fri Mar 03 2017 Ferry Huberts - 1.1.3
- Improve %post scriptlet
- Remove %preun scriptlet

* Tue Feb 28 2017 Ferry Huberts - 1.1.2
- Use masquerading by default.
  There is something on Fedora 25 that prevents non-masqueraded return
  traffic for routed connections. Return traffic does come back to the
  vpn server and onto bridge br0, but is somehow not forwarded to the
  tunnel interface.

* Wed Dec 14 2016 Ferry Huberts - 1.1.1
- Update 'Requires'

* Tue Dec 13 2016 Ferry Huberts - 1.1.0
- Use non-git-derived versions

* Mon Dec 12 2016 Ferry Huberts
- Bump to Fedora 25

* Thu Dec 10 2015 Ferry Huberts
- use %{name} directly
- clean up 'Requires'
- only support Fedora 22 and higher

* Fri Dec 04 2015 Ferry Huberts
- Use /usr/lib instead of /lib

* Tue May 26 2015 Ferry Huberts
- Removed the fedora-release dependency

* Tue Jan 13 2015 Ferry Huberts
- Initial release
