Geachte lezer,

Dit bericht bevat VPN certificaten die u in staat stellen een verbinding
te maken met de VPN server van __organisation__.

De volgende bestanden zijn mee gestuurd:
- De VPN configuratie bestanden : __zipfile__
- De installatie handleiding    : __manual__

Bewaar deze bestanden op uw systeem (bijvoorbeeld in de 'Downloads'
map), open vervolgens de installatie handleiding en volg de
instructies om de VPN verbinding te configureren en operationeel
te maken.


Stel uzelf alstublieft op de hoogte van de onderstaande
veiligheidswaarschuwing en verzeker uzelf ervan dat u de
waarschuwing begrijpt. Als u twijfelt, neem dan contact op
met uw VPN beheerder.

******************************************************************
* VPN connectie informatie is vertrouwelijk: communiceer nooit   *
* over deze informatie in een (onversleutelde) email of een in   *
* ander onversleuteld medium.                                    *
*                                                                *
* Het in bezit hebben van de certificaten is voldoende voor een  *
* derde partij om toegang te krijgen tot het netwerk dat         *
* verbonden is met de VPN server, er is geen wachtwoord nodig!   *
******************************************************************


Met vriendelijke groeten,

Uw VPN Beheerder
