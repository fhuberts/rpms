#!/usr/bin/bash

set -e
set -u

script="$0"
scriptName="$(basename "$script" 2> /dev/null)"

# Read the settings
source "/usr/share/vpn-server/vpn-server.conf"
source "$CFG_FUNCTIONSINCLUDE"




#
# Defines
#

CFG_CA_CRT="$CFG_OPENVPN_CA_DIR/$CFG_CA_KEYS_DIR/$CFG_CA_NAME.crt"




#
# Functions
#

# Let the user confirm before continuing
function confirm() {
  cat << EOF

#
# Confirm before continuing
#

Summary:
- Certificate         : $RUN_NAME
- E-Mail Address      : $RUN_EMAIL
- Protocols           : $RUN_PROTOCOLS_MENUENTRY
- Topologies          : $RUN_METHODS_MENUENTRY
- Client Platform     : $RUN_PLATFORM
- Language            : $RUN_LANGUAGE
- Manual              : $RUN_MANUAL
EOF

  cat << EOF

The program is going to use this information to create an encrypted ZIP file
with VPN connection information and email it to the specified email address.

EOF
  read -p "Press [Enter] to continue or CTRL-C to abort "
}


# Create the client configuration files in a subdirectory of a temporary
# directory. The manual is copied into the temporary directory itself.
#
# 1= The temporary directory
# 2= The subdirectory
# 3= The client platform (Linux/Mac/Windows)
function createClientConfigFiles() {
  local __tmpDir="$1"
  local __subDir="$2"
  local __platform="$3"

  cat << EOF

#
# Generating the client configuration files
#

EOF

  pushd "$__tmpDir" &> /dev/null

  # Copy manual
  if [[ -n "${RUN_MANUAL_FILE:-}" ]]; then
    cp "$RUN_MANUAL_FILE" .
  fi

  # Create and enter the subdirectory for files that will end up in the encrypted ZIP file
  mkdir -p "$__subDir"
  cd "$__subDir" &> /dev/null

  local __method=""
  local __protocol=""
  for __protocol in "${RUN_PROTOCOLS[@]}"; do
    for __method in "${RUN_METHODS[@]}"; do
      local __ovpnFile="$CFG_ORGANISATION - ${__protocol^^*} - ${__method^?}.ovpn"

      # Create the opvpn configuration file
      sed \
        -e "s#__protocol__#$__protocol#g" \
        -e "s#__device__#${CFG_METHOD_DEVICES[$__method]}#g" \
        -e "s#__gateway__#$CFG_VPN_GATEWAY#g" \
        -e "s#__port__#${CFG_OPENVPN_PORTS[$__protocol-$__method]}#g" \
        "$CFG_TEMPLATE_CLIENT" > "$__ovpnFile"

      if [[ "$__platform" == "$CFG_CLIENT_PLATFORM_WINDOWS" ]]; then
        # Windows
        cat >> "$__ovpnFile" << EOF

dev-node              $CFG_VPN_DEVICE_NAME_ON_WINDOWS
EOF
      fi

      cat >> "$__ovpnFile" << EOF

<ca>
EOF

cat "$CFG_CA_CRT" >> "$__ovpnFile"

      cat >> "$__ovpnFile" << EOF
</ca>

<cert>
EOF

cat "$RUN_CLIENT_CRT" >> "$__ovpnFile"

      cat >> "$__ovpnFile" << EOF
</cert>

<key>
EOF

cat "$RUN_CLIENT_KEY" >> "$__ovpnFile"

      cat >> "$__ovpnFile" << EOF
</key>
EOF
    done
  done

  chmod ug+rw,o+r-w *

  popd &> /dev/null
}


# Create an encrypted ZIP file from the client configuration files in a
# subdirectory of the temporary directory
#
# 1= The temporary directory
# 2= The subdirectory
# 3= The ZIP file to create
# 4= The password (when empty it will be asked)
function createEncryptedZipFile() {
  local __tmpDir="$1"
  local __subDir="$2"
  local __zipFile="$3"
  local __password="$(stringTrim "$4")"

  cat << EOF

#
# Creating an encrypted ZIP file with the client configuration files
#

EOF

  pushd "$__tmpDir" &> /dev/null

  if [[ -z "$__password" ]]; then
    zip -r --encrypt "$__zipFile" "$__subDir"
  else
    zip -r --encrypt "$__zipFile" --password "$__password" "$__subDir"
  fi

  popd &> /dev/null
}


# Send the ZIP file and the manual from a temporary directory
#
# 1= The temporary directory
# 2= The ZIP file to send
# 3= The manual to send (can be empty)
function emailZipFile() {
  local __tmpDir="$1"
  local __zipFile="$2"
  local __manualFile="$3"

  local -a __attachManual=( "" )
  local    __manualMsg=""
  local    __manualText="None"
  if [[ -n "$__manualFile" ]]; then
    __attachManual=( "-a" "$__manualFile" )
    __manualMsg=" and the manual"
    __manualText="$(basename "$__manualFile" 2> /dev/null)"
  fi

  cat << EOF

#
# Emailing the encrypted ZIP file$__manualMsg
#

EOF

  pushd "$__tmpDir" &> /dev/null

  cat "$RUN_EMAIL_TEMPLATE" | \
    sed -e "s#__organisation__#$CFG_ORGANISATION#g" \
        -e "s#__manual__#$__manualText#g" \
        -e "s#__zipfile__#$(basename "$__zipFile" 2> /dev/null)#g" | \
    mail \
      -S sendwait \
      -s "$CFG_EMAIL_SUBJECT" \
      -r "$CFG_EMAIL_SENDER" \
      -a "$__zipFile" \
      "${__attachManual[@]}" \
      "$RUN_EMAIL"

  cat << EOF

Note:
  Please remember to tell the recipient associated with the email address
    $RUN_EMAIL
  what the password of the ZIP file is.

  Tell the password in person or write it down and give the note in person.
  Make sure you and the recipient understand this security warning:

  ******************************************************************
  * VPN connection information is security sensitive: do not       *
  * communicate information about it by (unencrypted) email        *
  * or any other insecure medium.                                  *
  *                                                                *
  * Possession of the certificates is sufficient for a third party *
  * to obtain access to the network that is attached to the VPN    *
  * server, no passwords are needed!                               *
  ******************************************************************

EOF

  popd &> /dev/null
}


function usage() {
  local sc="$(sanitiseScriptName "$script")"

  cat << EOF

Usage:
  $sc [option]

    --name, -n <name>           Use <name> (case-insensitive) as the name of
                                the certificate.
    --protocol, -p <protocol>   Use <protocol> (case-insensitive) as the
                                connection protocol(s) to support. Choices are:
                                  udp, tcp, udp/tcp.
                                The recommended choice is 'udp'.
    --topology, -t <topology>   Use <topology> (case-insensitive) as the
                                connection topology(ies) to support. Choices
                                are:
                                  routed, bridged, routed/bridged.
                                The recommended choice is 'routed'.
    --platform, -o <platform>   Use <platform> (case-insensitive) as the
                                client platform operating system. Choices are:
                                  Android, Linux, Mac, Windows
    --manual, -m <manual>       Use <manual> (case-insensitive) as the
                                name of the client installation manual. Choices
                                depend on the '--name' and '--platform' options
                                and on installed packages with client
                                installation manuals.
                                An empty value signifies that no manual should
                                be sent.
    --passwordfile, -P <file>   Use <file> to retrieve the password for the
                                encrypted ZIP file from. The format of the file:
                                  name=password

    --help, -h                  This text.

The application will ask for all information that was not specified by using
the options.

EOF
}


# depends on: RUN_NAME
# sets:       RUN_EMAIL
function determineAndValidateEmail() {
  if [[ -z "$RUN_NAME" ]]; then
    return
  fi

  RUN_EMAIL="${DBEMAIL[$RUN_NAME]}"
  local __r=""
  checkEmailAddress "$RUN_EMAIL" "__r"
  if [[ -z "$__r" ]]; then
    exit 1
  fi
  RUN_EMAIL="$__r"
}


# depends on: RUN_NAME
# sets:       RUN_LANGUAGE
function determineAndValidateLanguage() {
  if [[ -z "$RUN_NAME" ]]; then
    return
  fi

  RUN_LANGUAGE="${DBEMAILLANGUAGE[$RUN_NAME]}"
  if [[ -z "$RUN_LANGUAGE" ]]; then
    RUN_LANGUAGE="$CFG_LANGUAGE_DEFAULT"
  fi
  if [[ -n "$RUN_LANGUAGE" ]]; then
    local __r=""
    checkLanguage "$RUN_LANGUAGE" "__r"
    if [[ -z "$__r" ]]; then
      exit 1
    fi
    RUN_LANGUAGE="$__r"
  fi
}


# depends on: RUN_LANGUAGE (-> RUN_NAME)
#             RUN_PLATFORM
#             RUN_MANUAL
# sets:       RUN_MANUALS
#             RUN_MANUALS_MENUENTRIES
#             RUN_MANUAL
#             RUN_MANUAL_FILE
function determineAndValidateManual() {
  if [[ -z "$RUN_LANGUAGE" ]] \
     || [[ -z "$RUN_PLATFORM" ]]; then
    return
  fi

  getManuals "$RUN_LANGUAGE" "$RUN_PLATFORM"

  if [[ -n "$RUN_MANUAL" ]]; then
    checkManualFile "$RUN_LANGUAGE" "$RUN_MANUAL" "RUN_MANUAL_FILE"
    if [[ -z "$RUN_MANUAL_FILE" ]]; then
      exit 1
    fi
    RUN_MANUAL="$(basename "$RUN_MANUAL_FILE" 2> /dev/null)"
  else
    RUN_MANUAL=""
    RUN_MANUAL_FILE=""
  fi
}


# depends on: RUN_NAME
#             RUN_PASSWORDFILE
# sets:       RUN_PASSWORD
function determineAndValidatePassword() {
  if [[ -z "$RUN_NAME" ]] \
     || [[ -z "$RUN_PASSWORDFILE" ]]; then
    return
  fi

  getPasswordFromFile "$RUN_PASSWORDFILE" "$RUN_NAME" "RUN_PASSWORD"
}


# depends on: RUN_NAME
#             RUN_PLATFORM
#             RUN_MANUAL
function determineAndValidateDerivedVariables() {
  determineAndValidateEmail
  determineAndValidateLanguage
  determineAndValidateManual
  determineAndValidatePassword
}




#
# Main
#


checkRoot
checkCADirectoryIsPresent
checkCADirectoryHasKeys

getClientKeys
getPlatforms
getLanguages
dbEmailRead


RUN_EMAIL=""
RUN_LANGUAGE=""
RUN_PASSWORD=""


# store CLI
storeCLI "$(sanitiseScriptName "$script")" "$@"

# transform long options into short options
declare -i index=0
for arg in "$@"; do
  shift
  index+=1
  case "$arg" in
    "--help")
      set -- "$@" "-h"
      ;;
    "--name")
      set -- "$@" "-n"
      ;;
    "--protocol")
      set -- "$@" "-p"
      ;;
    "--topology")
      set -- "$@" "-t"
      ;;
    "--platform")
      set -- "$@" "-o"
      ;;
    "--manual")
      set -- "$@" "-m"
      ;;
    "--passwordfile")
      set -- "$@" "-P"
      ;;
    "--"*)
      cliOptionError "$arg" $index "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
    *)
      set -- "$@" "$arg"
      ;;
  esac
done

# parse CLI
declare -i optionIndex=0
RUN_NAME=""
RUN_PROTOCOL=""
RUN_TOPOLOGY=""
RUN_PLATFORM=""
RUN_MANUAL=""
RUN_PASSWORDFILE=""
declare -i nameOption=0
declare -i protocolOption=0
declare -i topologyOption=0
declare -i platformOption=0
declare -i manualOption=0
declare -i passwordFileOption=0
while getopts ":hn:p:t:o:m:P:" option; do
  optionIndex+=1
  case "$option" in
    h) # --help
      usage
      exit 1
      ;;
    n) # --name
      RUN_NAME="$(stringTrim "${OPTARG,,}")"
      optionIndex+=1
      RUN_CLIENT_KEY="${RUN_CLIENT_KEYS_MAP[$RUN_NAME]:-}"
      if [[ -z "$RUN_CLIENT_KEY" ]]; then
        echo "ERROR: The certificate '$RUN_NAME' doesn't seem to exist."
        exit 1
      fi
      nameOption=1
      determineAndValidateDerivedVariables
      ;;
    p) # --protocol
      RUN_PROTOCOL="$(stringTrim "${OPTARG,,}")"
      optionIndex+=1
      checkProtocol "$RUN_PROTOCOL" "RUN_PROTOCOLS" "RUN_PROTOCOLS_MENUENTRY"
      if [[ -z "$RUN_PROTOCOLS_MENUENTRY" ]]; then
        exit 1
      fi
      protocolOption=1
      ;;
    t) # --topology
      RUN_TOPOLOGY="$(stringTrim "${OPTARG,,}")"
      optionIndex+=1
      checkMethod "$RUN_TOPOLOGY" "RUN_METHODS" "RUN_METHODS_MENUENTRY"
      if [[ -z "$RUN_METHODS_MENUENTRY" ]]; then
        exit 1
      fi
      topologyOption=1
      ;;
    o) # --platform
      RUN_PLATFORM="$(stringTrim "$OPTARG")"
      optionIndex+=1
      checkPlatform "$RUN_PLATFORM" "RUN_PLATFORM"
      if [[ -z "$RUN_PLATFORM" ]]; then
        exit 1
      fi
      platformOption=1
      determineAndValidateDerivedVariables
      ;;
    m) # --manual
      RUN_MANUAL="$(stringTrim "$OPTARG")"
      optionIndex+=1
      manualOption=1
      determineAndValidateDerivedVariables
      ;;
    P) # --passwordfile
      RUN_PASSWORDFILE="$(stringTrim "${OPTARG,,}")"
      optionIndex+=1
      passwordFileOption=1
      determineAndValidateDerivedVariables
      ;;
    *)
      cliOptionError "$option" $optionIndex "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))


declare -i asked=0


# handle name
if [[ $nameOption -eq 0 ]]; then
  askKey "RUN_CLIENT_KEY" "RUN_NAME"
  asked=1
fi


determineAndValidateDerivedVariables


# handle protocol
if [[ $protocolOption -eq 0 ]]; then
  askProtocols "RUN_PROTOCOLS" "RUN_PROTOCOLS_MENUENTRY"
  asked=1
fi


# handle topology
if [[ $topologyOption -eq 0 ]]; then
  askMethods "RUN_METHODS" "RUN_METHODS_MENUENTRY"
  asked=1
fi


# handle platform
if [[ $platformOption -eq 0 ]]; then
  askPlatform "RUN_PLATFORM"
  asked=1
fi


determineAndValidateDerivedVariables


# handle manual
if [[ $manualOption -eq 0 ]]; then
  askManual "RUN_MANUAL_FILE" "RUN_MANUAL"
  asked=1
fi


RUN_EMAIL_TEMPLATE="$(getEmailTemplate "$scriptName" "" "$RUN_LANGUAGE")"
if [[ -z "$RUN_EMAIL_TEMPLATE" ]]; then
  echo "ERROR: email template not found for language '$CFG_LANGUAGE_DEFAULT'"
  exit 1
fi

# Derive the keys prefix from the certificate organisation
CFG_KEYS_PREFIX="$(echo "${CFG_ORGANISATION,,}" | sed -r 's/[^a-zA-Z0-9]//g' 2> /dev/null)"


# Derive the client certificate from the client key
RUN_CLIENT_CRT="$(echo "${RUN_CLIENT_KEY%\.key}.crt" | sed "s#/$CFG_CA_PRIVATE_DIR/#/$CFG_CA_ISSUED_DIR/#" 2> /dev/null)"
RUN_ZIP_FILE="$CFG_KEYS_PREFIX-$RUN_NAME.zip"


if [[ $asked -ne 0 ]]; then
  confirm
fi


SCRATCHDIR="$(mktemp -d 2> /dev/null)"
createClientConfigFiles "$SCRATCHDIR" "$CFG_KEYS_PREFIX-$RUN_NAME" "$RUN_PLATFORM"
createEncryptedZipFile  "$SCRATCHDIR" "$CFG_KEYS_PREFIX-$RUN_NAME" "$RUN_ZIP_FILE" "$RUN_PASSWORD"
emailZipFile            "$SCRATCHDIR" "$RUN_ZIP_FILE" "${RUN_MANUAL_FILE:-}"
rm -fr "$SCRATCHDIR"
SCRATCHDIR=""

exit 0
