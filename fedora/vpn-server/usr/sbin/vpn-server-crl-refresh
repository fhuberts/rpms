#!/usr/bin/bash

set -e
set -u

script="$0"

# Read the settings
source "/usr/share/vpn-server/vpn-server.conf"
source "$CFG_FUNCTIONSINCLUDE"




#
# Functions
#

function usage() {
  local sc="$(sanitiseScriptName "$script")"

  cat << EOF

Usage:
  $sc [option]

    --help, -h    This text.

EOF
}




#
# Main
#


checkRoot
if [[ ! -d "$CFG_OPENVPN_CA_DIR/$CFG_CA_KEYS_DIR" ]]; then
  exit 0
fi


# store CLI
storeCLI "$(sanitiseScriptName "$script")" "$@"

# transform long options into short options
declare -i index=0
for arg in "$@"; do
  shift
  index+=1
  case "$arg" in
    "--help")
      set -- "$@" "-h"
      ;;
    "--"*)
      cliOptionError "$arg" $index "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
    *)
      set -- "$@" "$arg"
      ;;
  esac
done

# parse CLI
declare -i optionIndex=0
while getopts ":h" option; do
  optionIndex+=1
  case "$option" in
    h) # --help
      usage
      exit 1
      ;;
    *)
      cliOptionError "$option" $optionIndex "${CMDLINEARGS[@]}"
      usage
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))


set +e
doOpenVPNService "stop"
set -e

updateCrl
copyCRLInto "$CFG_MODE_SETUP"

set +e
doOpenVPNService "start"
set -e

exit 0
