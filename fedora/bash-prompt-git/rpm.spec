%global gitRpmName        git-core
%global profiledLinkFile  %{_sysconfdir}/profile.d/%{name}-link.sh

Name:             bash-prompt-git
Version:          1.3.9
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Show Git information in the bash shell prompt

Requires:         %{gitRpmName}

Requires:         bash
Requires:         coreutils
Requires:         git, grep
Requires:         rpm


%description
Configuration files are added to the system-wide bash profile directory to show
Git information in the bash shell prompt.

Adjust the file
  %{_sysconfdir}/profile.d/bash-prompt-git-settings.sh
to your preferences.

Leave the symbolic link
  %{profiledLinkFile}
untouched.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *

touch "%{buildroot}%{profiledLinkFile}"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

IFS=$'\n'
complFile="$(rpm -q -l "%{gitRpmName}" 2> /dev/null | \
             grep -E '/git-prompt\.sh$' 2> /dev/null | \
             grep -v '/doc/' 2> /dev/null | \
             tail -1 2> /dev/null)"
rm -f "%{profiledLinkFile}"
if [[ -n "$complFile" ]]; then
  ln -s "$complFile" "%{profiledLinkFile}"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  rm -f "%{profiledLinkFile}"
fi

exit 0


%triggerin -p /usr/bin/bash -- %{gitRpmName}
# $1 The number of instances of the source package (this package, a.k.a. the
#    triggered package) which will remain when the trigger has completed
# $2 The number of instances of the target package (git-core) which will remain
if [[ $2 -eq 0 ]]; then
  rm -f "%{profiledLinkFile}"
  exit 0
fi

IFS=$'\n'
complFile="$(rpm -q -l "%{gitRpmName}" 2> /dev/null | \
             grep -E '/git-prompt\.sh$' 2> /dev/null | \
             grep -v '/doc/' 2> /dev/null | \
             tail -1 2> /dev/null)"
rm -f "%{profiledLinkFile}"
if [[ -n "$complFile" ]]; then
  ln -s "$complFile" "%{profiledLinkFile}"
fi

exit 0


%triggerun -p /usr/bin/bash -- %{gitRpmName}
# $1 The number of instances of the source package (this package, a.k.a. the
#    triggered package) which will remain when the trigger has completed
# $2 The number of instances of the target package (git-core) which will remain
if [[ $2 -eq 0 ]]; then
  rm -f "%{profiledLinkFile}"
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/profile.d/*
%ghost %{profiledLinkFile}


%changelog
* Sat Apr 17 2021 Ferry Huberts - 1.3.9
- Update the bash-prompt-git-settings.sh settings
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.3.8
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.3.7
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.3.6
- Bump to Fedora 27

* Wed Mar 01 2017 Ferry Huberts - 1.3.5
- Improve scriptlets

* Fri Dec 02 2016 Ferry Huberts - 1.3.4
- Bump to Fedora 25

* Sun Apr 24 2016 Ferry Huberts - 1.3.3
- require git-core again

* Wed Dec 23 2015 Ferry Huberts - 1.3.2
- minor fix to the spec file

* Tue Dec 15 2015 Ferry Huberts - 1.3.1
- revert back to using PS1 instead of PROMPT_COMMAND;
  PROMPT_COMMAND is faster but gnome-terminal doesn't seem to respect it

* Mon Dec 14 2015 Ferry Huberts - 1.3.0
- the dependency on git-core is weak; the package can handle git-core being
  uninstalled and installed later

* Mon Dec 14 2015 Ferry Huberts - 1.2.2
- add forgotten dependency on rpm

* Sat Dec 12 2015 Ferry Huberts - 1.2.1
- fix trigger scripts

* Sat Dec 12 2015 Ferry Huberts - 1.2.0
- update to latest git
- use the faster BASH_PROMPT method to set the bash prompt
- ensure the RPM triggers use the correct target package

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- clean up 'Requires'
- only support Fedora 23 and higher

* Fri Nov 13 2015 Ferry Huberts - 1.0.1
- Update for Fedora 23

* Sun Jan 25 2015 Ferry Huberts - 1.0.0
- Initial release
