Name:             dnf-automatic-updates-enable
Version:          1.0.12
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Enable automatic updates with dnf

Requires:         configfile-utils
Requires:         dnf-automatic
Requires:         systemd


%description
Enable automatic updates with dnf and disable automatic updates on uninstall.


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  configfile-adjust-settings \
    -s -f "%{_sysconfdir}/dnf/automatic.conf" \
    "apply_updates=yes"

  systemctl -q enable            "dnf-automatic-install.timer"
  systemctl -q reload-or-restart "dnf-automatic-install.timer"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  configfile-adjust-settings \
    -s -f "%{_sysconfdir}/dnf/automatic.conf" \
    "apply_updates=no"

  systemctl -q stop    "dnf-automatic-install.timer"
  systemctl -q disable "dnf-automatic-install.timer"
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Sat Apr 17 2021 Ferry Huberts - 1.0.12
- Improve the spec file
- Tweak starting services

* Wed Mar 03 2016 Ferry Huberts - 1.0.11
- Only touch the apply_updates setting, and restore on uninstall

* Sun Jul 05 2020 Ferry Huberts - 1.0.10
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.9
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.8
- Bump to Fedora 27
- Also adjust download_updates setting

* Thu Jul 27 2017 Ferry Huberts - 1.0.7
- Update for Fedora 26

* Wed Mar 01 2017 Ferry Huberts - 1.0.6
- Improve %post scriplet
- Remove %preun scriplet

* Wed Dec 28 2016 Ferry Huberts - 1.0.5
- systemctl changes

* Fri Dec 23 2016 Ferry Huberts - 1.0.4
- systemctl changes

* Fri Dec 02 2016 Ferry Huberts - 1.0.3
- Bump to Fedora 25

* Sat May 21 2016 Ferry Huberts - 1.0.2
- systemctl changes

* Wed May 04 2016 Ferry Huberts - 1.0.1
- Disable automatic updates on uninstall

* Wed May 04 2016 Ferry Huberts - 1.0.0
- Initial release
