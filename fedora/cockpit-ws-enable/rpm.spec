Name:             cockpit-ws-enable
Version:          1.0.7
Release:          2%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Enable the cockpit system, including firewall rules

Requires:         cockpit-utils

Requires:         cockpit-ws
Requires:         firewalld
Requires:         systemd


%description
Enable the cockpit system including firewall rules.


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  cockpitPort=$(cockpit-get-port)

  if [[ $cockpitPort -gt 0 ]]; then
    for i in "" "--permanent"; do
      firewall-cmd -q $i --add-port=$cockpitPort/tcp
    done

    systemctl -q enable            "cockpit.socket"
    systemctl -q reload-or-restart "cockpit.socket"
  fi
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  cockpitPort=$(cockpit-get-port)

  if [[ $cockpitPort -gt 0 ]]; then
    for i in "" "--permanent"; do
      firewall-cmd -q $i --remove-port=$cockpitPort/tcp
    done

    systemctl -q stop    "cockpit.socket"
    systemctl -q disable "cockpit.socket"
  fi
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Sat Apr 17 2021 Ferry Huberts - 1.0.7
- Improve the spec file
- Tweak starting services

* Sun Jul 05 2020 Ferry Huberts - 1.0.6
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.5
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.4
- Bump to Fedora 27
- Update the description

* Wed Mar 01 2017 Ferry Huberts - 1.0.3
- Improve %post scriptlet
- Remove %preun scriptlet

* Fri Dec 02 2016 Ferry Huberts - 1.0.2
- Disable on uninstall

* Fri Dec 02 2016 Ferry Huberts - 1.0.1
- Bump to Fedora 25

* Tue Apr 26 2016 Ferry Huberts - 1.0.0
- Initial release, split off of base-server 1.6.5
