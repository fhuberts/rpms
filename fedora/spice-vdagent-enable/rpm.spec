Name:             spice-vdagent-enable
Version:          1.0.7
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          Enables or disables the spice agent when relevant

Requires:         grep
Requires:         spice-vdagent, systemd


%description
Enables or disables the spice agent when relevant:
- enables when the machine is a virtual machine
- disables when the machine is NOT a virtual machine


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

if [[ $1 -eq 1 ]]; then
  # install

  machineIsVm=0
  if [[ -n "$(grep "hypervisor" "/proc/cpuinfo" 2> /dev/null)" ]]; then
    # virtual machine
    machineIsVm=1
  fi

  # physical machine: stop   and disable
  # virtual  machine: enable and restart
  if [[ $machineIsVm -eq 0 ]]; then
    # physical machine

    systemctl -q stop              "spice-vdagentd.service"
    systemctl -q disable           "spice-vdagentd.service"
  else
    # virtual machine

    systemctl -q enable            "spice-vdagentd.service"
    systemctl -q reload-or-restart "spice-vdagentd.service"
  fi
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Mon Apr 19 2021 Ferry Huberts - 1.0.7
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.0.6
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.5
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.4
- Bump to Fedora 27
- Update description

* Fri Mar 03 2017 Ferry Huberts - 1.0.3
- Improve %post scriptlet
- Remove %preun scriptlet

* Fri Dec 02 2016 Ferry Huberts - 1.0.2
- Disable on uninstall

* Fri Dec 02 2016 Ferry Huberts - 1.0.1
- Bump to Fedora 25

* Thu Apr 28 2016 Ferry Huberts - 1.0.0
- Initial release, split off of base-server 1.6.8
