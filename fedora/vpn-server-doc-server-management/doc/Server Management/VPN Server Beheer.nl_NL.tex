\newcommand{\myTargetOS}{}

\newcommand{\myLanguage}{dutch}
\newcommand{\myPageSize}{a4paper}
\newcommand{\myDocTitle}{VPN~Server~Beheer}
\newcommand{\myFooterOf}{van}
\newcommand{\myFooterSeparator}{}


%-------------------------------------------------------------------------------
% Template: includes \begin{document} but not \end{document}
%-------------------------------------------------------------------------------

\input{../../vpn-server-doc.build/template.texinc}


%-------------------------------------------------------------------------------
% Introduction
%-------------------------------------------------------------------------------

\newpage
\section{Inleiding}

\par
Dit document beschrijft het beheer van de VPN~Server.

De VPN~Server is een OpenVPN~server: OpenVPN is de defacto standaard in
de industrie voor veilige VPN~servers.

\keepTogether{
  In het kort komt de werking van de VPN~server ruwweg op de volgende procedure
  neer:

  \begin{itemize}
    \item Een gebruiker machine maakt een versleutelde verbinding met de
          VPN~Server.
    \item De VPN~Server vraagt de gebruiker machine om zich te authentiseren
          met zijn certificaat.
    \item De gebruiker machine stuurt zijn certificaat naar de VPN~Server.
    \item De VPN~Server valideert in zijn administratie dat het certificaat
          niet ingetrokken is.
    \item De versleutelde verbinding wordt verder opgebouwd alleen indien het
          certificaat niet ingetrokken is. Hierbij zal de gebruiker machine
          verbonden worden met het netwerk op de locatie van de VPN~server.
  \end{itemize}
}

\par
Het beheer van de VPN~server bestaat voornamelijk uit het aanmaken en
intrekken van certificaten voor gebruiker machines, hetgeen dit document
beschrijft.

Echter, landurig in gebruik zijnde VPN~Servers zullen te maken krijgen met
certificaten die verlopen. Het vernieuwen van certificaten is ook beschreven
in dit document.

\par
\textbf{Attentie}:\\
VPN~connectie informatie is vertrouwelijk: communiceer nooit over deze
informatie in een (onversleutelde) email of in een ander onversleuteld medium.

Het in bezit hebben van de certificaten is voldoende voor een derde partij om
toegang te krijgen tot het netwerk dat verbonden is met de VPN~server, er is
geen wachtwoord nodig!

Het is daarom van groot belang dat de certificaten netjes beheerd worden en dat
het VPN~Server wachtwoord geheim blijft.

Indien de certificaten van een gebruiker verloren raken (bijvoorbeeld door
diefstal of verlies van een gebruiker machine) dan dienen de certificaten van
de betreffende gebruiker onmiddellijk ingetrokken te worden. Dit geldt ook
als een gebruiker niet langer in dienst is.


%-------------------------------------------------------------------------------
% Management of Certificates
%-------------------------------------------------------------------------------

\newpage
\section{Beheer~van~Certificaten}

\par
Om een gebruiker machine toegang te geven (of te weigeren) tot het netwerk van
de locatie is het aanmaken (intrekken) van een gebruiker certificaat vereist.

\keepTogether{
  Elke gebruiker machine heeft een eigen certificaat.

  \begin{itemize}
    \item Om gebruiker machine X toegang te geven moet er een certificaat voor
          die machine gegenereerd worden.
    \item Om gebruiker machine X de toegang te weigeren moeten het bijbehorende
          certificaat ingetrokken worden.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Creating A Certificate
%-------------------------------------------------------------------------------

\subsection{Aanmaken~van~een~Certificaat}

\par
\keepTogether{
  Vraag aan de gebruiker voor wie het certificaat gegenereerd wordt wat
  zijn/haar email adres is en wat zijn/haar taalvoorkeur is.

  Vraag ook welk besturingssysteem (inclusief de versie van het
  besturingssysteem) de machine gebruikt die het certificaat gaat gebruiken.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Maak het gebruiker certificaat aan (volg de instructies op het
          scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-build.lst}
    \item Stuur de installatie handleiding en de VPN~configuratie bestanden naar
          de gebruiker (volg de instructies op het scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-send.lst}
    \item Breng de gebruiker \textbf{mondeling} op de hoogte van het wachtwoord
          van het gegenereerde ZIP~bestand, \textbf{nooit} per email of op een
          andere manier die onveilig (niet versleuteld) is.
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Listing Certificates
%-------------------------------------------------------------------------------

\subsection{Overzicht~van~Certificaten}

\par
\keepTogether{
  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Laat het overzicht van alle uitgegeven gebruiker certificaten en de
          bijbehorende informatie zien:
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-list.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Changing Certificate Information
%-------------------------------------------------------------------------------

\subsection{Certificaat~Informatie~Wijzigen}

\par
\keepTogether{
  Als een gebruiker aan wie een gebruiker certificaat is uitgegeven zijn/haar
  email adres of taalvoorkeur wijzigt, dan dient dit bijgewerkt te worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Wijzig het email adres dat bij het uitgegeven gebruiker certificaat
          hoort (volg de instructies op het scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-change.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Revoking Certificates
%-------------------------------------------------------------------------------

\subsection{Intrekken~van~Certificaten}

\par
\keepTogether{
  Indien een certificaat van een gebruiker verloren raakt (bijvoorbeeld door
  diefstal of verlies van een gebruiker machine) of de gebruiker gaat uit
  dienst, dan dient het certificaat van de betreffende gebruiker onmiddellijk
  ingetrokken te worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Trek het betreffende certificaat in (volg de instructies op het
          scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-revoke.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% Expiration of Certificates
%-------------------------------------------------------------------------------

\newpage
\section{Verlopen~van~Certificaten}

\par
Alle certificaten die uitgegeven zijn zullen op enig moment verlopen, echter
allen op een ander moment. De verloopdatum verschilt per type certificaat en
hangt ook af van het moment van aanmaken van het certificaat.


%-------------------------------------------------------------------------------
% + Expiration Checks
%-------------------------------------------------------------------------------

\subsection{Verloopdata~Controles}

\par
Om de verloopdata van alle uitgegeven certificaten in de gaten te houden
wordt er automatisch elke maand een controle uitgevoerd. Deze controle zal
de contactpersoon van de organisatie per email informeren over de certificaten
die op korte termijn zullen gaan verlopen. De email zal per certificaat ook
vermelden vanaf wanneer het certificaat vernieuwd kan worden, en de email zal
ook instructies hiervoor bevatten.

\keepTogether{
\par
  De verloopdata controles kunnen ook handmatig uitgevoerd worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Voer de verloopdata controles uit:
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-expiration-checks.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing A Client Certificate
%-------------------------------------------------------------------------------

\subsection{Vernieuwen~van~een~Gebruiker~Certificaat}

\par
\keepTogether{
  In het geval een gebruiker certificaat op korte termijn verloopt (de
  contactpersoon van de organisatie zal hiervan per email geinformeerd worden)
  dan dient het vernieuwd te worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Vernieuw het gebruiker certificaat (volg de instructies op het
          scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-client.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing The Server Certificate
%-------------------------------------------------------------------------------

\subsection{Vernieuwen~van~het~Server~Certificaat}

\par
\keepTogether{
  In het geval het server certificaat op korte termijn verloopt (de
  contactpersoon van de organisatie zal hiervan per email geinformeerd worden)
  dan dient het vernieuwd te worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Vernieuw het server certificaat (volg de instructies op het scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-server.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing The CA Certificate
%-------------------------------------------------------------------------------

\subsection{Vernieuwen~van~het~CA~Certificaat}

\par
\keepTogether{
  In het geval het OpenVPN CA (Certificate Authority) certificaat op korte
  termijn verloopt (de contactpersoon van de organisatie zal hiervan per email
  geinformeerd worden) dan dient het vernieuwd te worden.

  \begin{itemize}
    \item Login op de VPN~Server onder het \textbf{root} account.
    \item Vernieuw het CA certificaat (volg de instructies op het scherm):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-ca.lst}
    \item Log uit van de VPN~Server door \textbf{logout} of
          \textbf{\mbox{Ctrl-d}} te typen.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% End of document
%-------------------------------------------------------------------------------

\end{document}
