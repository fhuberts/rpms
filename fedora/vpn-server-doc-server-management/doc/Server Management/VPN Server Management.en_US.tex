\newcommand{\myTargetOS}{}

\newcommand{\myLanguage}{english}
\newcommand{\myPageSize}{a4paper}
\newcommand{\myDocTitle}{VPN~Server~Management}
\newcommand{\myFooterOf}{of}
\newcommand{\myFooterSeparator}{}


%-------------------------------------------------------------------------------
% Template: includes \begin{document} but not \end{document}
%-------------------------------------------------------------------------------

\input{../../vpn-server-doc.build/template.texinc}


%-------------------------------------------------------------------------------
% Introduction
%-------------------------------------------------------------------------------

\newpage
\section{Introduction}

\par
This document describes the management of the VPN~Server.

The VPN~Server is an OpenVPN~server: OpenVPN is the defacto standard in
the industry for safe VPN~servers.

\keepTogether{
  In short, the VPN~server roughly operates as follows:

  \begin{itemize}
    \item A client machine initiates an encrypted connection with the
          VPN~Server.
    \item The VPN~Server asks the client machine to authenticate itself with
          its certificate.
    \item The client machine sends its certificate to the VPN~Server.
    \item The VPN~Server validates in its administration that the certificate
          is not revoked.
    \item The encrypted connection is further established only when the
          certificate is not revoked. The client machine will be connected to
          the network at the location of VPN~server.
  \end{itemize}
}

\par
The management of the VPN~server mainly comprises creating and revoking
certificates of client machines, which is what this document describes.

However, long lived VPN~Servers will experience expiring certificates.
Renewing certificates is also described in this document.

\par
\textbf{Attention}:\\
VPN~connection information is security sensitive: do not communicate
information about it by (unencrypted) email or by any other insecure medium.

Possession of the certificates is sufficient for a third party to obtain
access to the network that is attached to the VPN~server, no password is needed!

It is therefore of the utmost importance that the certificates are properly
managed and that the VPN~Server password remains secret.

In case client certificates are lost (for example through theft or loss of a
client machine) then those certificates must be revoked immediately. This also
applies when a user is no longer employed.


%-------------------------------------------------------------------------------
% Management of Certificates
%-------------------------------------------------------------------------------

\newpage
\section{Management~of~Certificates}

\par
To let a client machine obtain access (or deny it) to the network that is
attached to the VPN~server, a certificate needs to be created (or revoked).

\keepTogether{
  Every client machine has its own certificate.

  \begin{itemize}
    \item To grant client machine X access, a certificate needs to be created
          for that machine.
    \item To deny client machine X access, its certificate needs to be revoked.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Creating A Certificate
%-------------------------------------------------------------------------------

\subsection{Creating~A~Certificate}

\par
\keepTogether{
  Ask the user for whom a certificate will be created what his/her email address
  is and what his/her language preference is.

  Also ask which operating system (including the version of the operating
  system) the machine uses that will use the certificate.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Create the certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-build.lst}
    \item Send the installation manual and the VPN~configuration files to the
          user (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-send.lst}
    \item Notify the user \textbf{by~word~of~mouth} of the password of the
          generated ZIP~file, \textbf{never} by email or by any other
          unsafe (unencrypted) means.
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Listing Certificates
%-------------------------------------------------------------------------------

\subsection{Listing~Certificates}

\par
\keepTogether{
  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item List all issued client certificates with the associated information:
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-list.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Changing Certificate Information
%-------------------------------------------------------------------------------

\subsection{Changing~Certificate~Information}

\par
\keepTogether{
  When a user to which a client certificate was issued changes his/her email
  address or language preference, then this must be updated.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Change the email address that is associated with the client
          certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-change.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Revoking Certificates
%-------------------------------------------------------------------------------

\subsection{Revoking~Certificates}

\par
\keepTogether{
  In case a client certificate is lost (for example through theft or loss of a
  client machine) then its certificate must be revoked immediately. This also
  applies when a user is no longer employed.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Revoke the relevant certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-revoke.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% Expiration of Certificates
%-------------------------------------------------------------------------------

\newpage
\section{Expiration~of~Certificates}

\par
All certificates that are issued will expire at a certain moment, albeit each
at different moments. The expiration dates differ for the different types of
certificates and also depend on the moment of creation of a certificate.


%-------------------------------------------------------------------------------
% + Expiration Checks
%-------------------------------------------------------------------------------

\subsection{Expiration~Checks}

\par
To monitor the expiration dates of all issued certificates, a monthly check is
automatically run. This check will inform the organisation's contact per email
of the certificates that are about to expire and from which date each
certificate can be renewed. The email will also contain instructions on how
to proceed.

\keepTogether{
\par
  The expiration checks can also be run manually.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Run the expiration checks:
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-expiration-checks.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing A Client Certificate
%-------------------------------------------------------------------------------

\subsection{Renewing~A~Client~Certificate}

\par
\keepTogether{
  In case a client certificate is about to expire (the organisation's contact
  will be notified per email about this) it needs to be renewed.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Renew the client certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-client.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing The Server Certificate
%-------------------------------------------------------------------------------

\subsection{Renewing~The~Server~Certificate}

\par
\keepTogether{
  In case the server certificate is about to expire (the organisation's contact
  will be notified per email about this) it needs to be renewed.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Renew the server certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-server.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% + Renewing The CA Certificate
%-------------------------------------------------------------------------------

\subsection{Renewing~The~CA~Certificate}

\par
\keepTogether{
  In case the OpenVPN CA (Certificate Authority) certificate is about to expire
  (the organisation's contact will be notified per email about this) it needs
  to be renewed.

  \begin{itemize}
    \item Login on the VPN~Server under the \textbf{root} account.
    \item Renew the CA certificate (follow the instructions on the screen):
          \lstinputlisting[language=bash, frame=single]{vpn-server-certificate-renew-ca.lst}
    \item Logout from the VPN~Server by typing \textbf{logout} or
          \textbf{\mbox{Ctrl-d}}.
  \end{itemize}
}


%-------------------------------------------------------------------------------
% End of document
%-------------------------------------------------------------------------------

\end{document}
