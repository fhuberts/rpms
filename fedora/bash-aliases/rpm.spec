Name:             bash-aliases-shell
Version:          1.5.7
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Several bash aliases

Requires:         bash
Requires:         coreutils


%package -n bash-aliases-git-cli
Summary:          Several bash aliases and small utilities for git CLI tools

Requires:         bash, bash-completion-pelagic-helpers
Requires:         coreutils
Requires:         gawk, git, git-core, git-core-doc
Recommends:       git-extras, git-email, git-lfs, git-publish, git-repair, grep

Suggests:         libnotify


%package -n bash-aliases-git-gui
Summary:          Several bash aliases and small utilities for git CLI and GUI tools

Requires:         bash, bash-aliases-git-cli
Requires:         git, git-gui, gitk

Obsoletes:        bash-aliases-git <= 1.1.0


%package -n bash-aliases-dnf
Summary:          Several bash aliases for dnf

Requires:         bash
Requires:         dnf
Requires:         sudo

Obsoletes:        bash-aliases-yum < 1.4.3


%package -n bash-ccompilersuite
Summary:          Several bash helpers for C/C++ compiler suites

Requires:         bash
Requires:         grep
Requires:         sed


%description
Several bash aliases.


%description -n bash-aliases-git-cli
Several bash aliases and small utilities for git CLI tools.


%description -n bash-aliases-git-gui
Several bash aliases and small utilities for git CLI and GUI tools.


%description -n bash-aliases-dnf
Several bash aliases for dnf.


%description -n bash-ccompilersuite
Several bash helpers for C/C++ compiler suites


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


%files
%defattr(-,root,root)
%config %{_sysconfdir}/profile.d/bash-aliases-shell.sh


%files -n bash-aliases-git-cli
%defattr(-,root,root)
%config %{_sysconfdir}/profile.d/bash-aliases-git-cli.sh
%{_bindir}/*
%{_datarootdir}/bash-aliases/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/pixmaps/*


%files -n bash-aliases-git-gui
%defattr(-,root,root)
%config %{_sysconfdir}/profile.d/bash-aliases-git-gui.sh


%files -n bash-aliases-dnf
%defattr(-,root,root)
%config %{_sysconfdir}/profile.d/bash-aliases-dnf.sh


%files -n bash-ccompilersuite
%defattr(-,root,root)
%config %{_sysconfdir}/profile.d/ccompilersuite.sh


%changelog
* Tue Oct 22 2024 Ferry Huberts - 1.5.7
- Let git-range-command notifications be transient

* Sun Sep 03 2023 Ferry Huberts - 1.5.6
- Add bash helpers for C/C++ compiler suites

* Fri Dec 30 2022 Ferry Huberts - 1.5.5
- Add an extra force to git clean

* Mon Jul 12 2021 Ferry Huberts - 1.5.4
- Improved the completion for git-range-command

* Sat Apr 17 2021 Ferry Huberts - 1.5.3
- Improved the icon for git-range-command
- Improve the git-range-command script
- Add extra git dependencies

* Wed Feb 24 2021 Ferry Huberts - 1.5.2
- Move some bash functions to an include file

* Sun Feb 21 2021 Ferry Huberts - 1.5.1
- Rework and simplify bash completion

* Fri Feb 19 2021 Ferry Huberts - 1.5.0
- Improve usage and add bash completion for git-range-command
- Bump to Fedora 33

* Sun Jul 05 2020 Ferry Huberts - 1.4.12
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.4.11
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.4.10
- Bump to Fedora 27
- Fix a typo in the git-range-command script

* Mon Apr 17 2017 Ferry Huberts - 1.4.9
- Use bash' [[ builtin instead of the program [

* Mon Mar 06 2017 Ferry Huberts - 1.4.8
- Add a gitrbi alias

* Mon Mar 06 2017 Ferry Huberts - 1.4.7
- Refactor gitt script into a profile.d function
- Refactor gitwipe alias into a function

* Wed Dec 14 2016 Ferry Huberts - 1.4.6
- Update 'Requires'

* Tue Dec 13 2016 Ferry Huberts - 1.4.5
- Fix rpmlint warnings

* Thu Dec 08 2016 Ferry Huberts - 1.4.4
- Fix usage of the git-range-command pixmap

* Fri Dec 02 2016 Ferry Huberts - 1.4.3
- Bump to Fedora 25
- Remove bash-aliases-yum, obsoleted by bash-aliases-dnf
- Move git-range-command pixmap into a clearly recognisable subdirectory

* Sat Apr 16 2016 Ferry Huberts - 1.4.2
- git-range-command: add libnotify support

* Tue Dec 22 2015 Ferry Huberts - 1.4.1
- fix dependencies for bash-aliases-git-cli

* Mon Dec 14 2015 Ferry Huberts - 1.4.0
- the dependency on bash-aliases-git-cli is weak; the package can handle
  bash-aliases-git-cli being uninstalled and installed later

* Sat Dec 12 2015 Ferry Huberts - 1.3.0
- improve the git-range script

* Fri Dec 11 2015 Ferry Huberts - 1.2.0
- Remove bash-aliases-git package

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- clean up 'Requires'
- only support Fedora 23 and higher

* Mon Nov 30 2015 Ferry Huberts - 1.0.1
- Add git-range-command to bash-aliases-git-cli

* Thu Jun 25 2015 Ferry Huberts - 1.0.0-2
- Add bash-aliases-dnf package

* Sun Jan 25 2015 Ferry Huberts - 1.0.0
- Initial release
