function ccompilersuite() {
  local -i argc=$#
  if [[ $argc -eq 0 ]]; then
    local exp="$(export -p | grep -E '(CC|CXX)=' | sed -r 's/^/  /')"
    cat << EOF
CC  = ${CC:-<none>}
CXX = ${CXX:-<none>}

Exported:
${exp:-<none>}
EOF
    return
  fi

  if [[ $argc -gt 1 ]]; then
    cat << EOF
Specify one of the following options:
  gcc     Force GCC   as the C and C++ compilers
  clang   Force Clang as the C and C++ compilers
  -       Remove forcing the C and C++ compilers
EOF
    return
  fi

  local suite="${1,,}"

  if [[ "$suite" == "gcc" ]]; then
    export CC="gcc"
    export CXX="g++"
  elif [[ "$suite" == "clang" ]]; then
    export CC="clang"
    export CXX="clang++"
  elif [[ "$suite" == "-" ]]; then
    export -n CC
    export -n CXX
    unset CC
    unset CXX
  else
    echo "ERROR: unknown compiler suite"
    ccompilersuite 1 2 3 4 5
    return
  fi

  ccompilersuite
}

