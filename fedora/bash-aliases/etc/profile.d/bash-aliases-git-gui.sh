alias gitg='git gui'
alias gitka='gitk --all'

function gitt {
  git gui &
  sleep 1
  if [[ $# -eq 0 ]]; then
    gitk --all &
  else
    gitk $* &
  fi
}
