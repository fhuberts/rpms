alias gitmt='git mergetool'

alias gitrba='git rebase --abort'
alias gitrbc='git rebase --continue'
alias gitrbi='git rebase --interactive'
alias gitrbs='git rebase --skip'

function gitwipe {
  git reset --hard
  git clean -ffdx $*
}
