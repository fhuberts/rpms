Name:             bash-proxy
Version:          1.0.5
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Proxy server configuration for bash

Requires:         bash


%description
Proxy server configuration for bash.

Adjust %{_sysconfdir}/profile.d/bash-proxy.sh to use a proxy.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/profile.d/*


%changelog
* Tue Apr 20 2021 Ferry Huberts - 1.0.5
- Add socks_proxy and no_proxy settings

* Sat Apr 17 2021 Ferry Huberts - 1.0.4
- Improve the spec file

* Sun Jul 05 2020 Ferry Huberts - 1.0.3
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.2
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.0.1
- Bump to Fedora 27

* Wed Dec 21 2016 Ferry Huberts - 1.0.0
- Initial version
