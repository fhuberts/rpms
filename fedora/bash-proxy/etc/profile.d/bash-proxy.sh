#
# Uncomment and adjust the lines below to use a proxy
#

#export http_proxy="http://ADDRESS:PORT"
#export https_proxy="$http_proxy"
#export ftp_proxy="$http_proxy"
#export socks_proxy="$http_proxy"
#export no_proxy="localhost,localhost.localdomain,127.0.0.1,::1"
