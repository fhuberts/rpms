#
# These settings are used in a virtual machine to create the
# screenshots for the client installation manuals.
#


#
# Adjust these settings to match your site
#


# Network bridge settings
CFG_BRIDGE_IF="br-vpn"              # The bridge to create/use for the bridged VPN services
CFG_ETH_IF="enp1s0"                 # The Ethernet interface to make a member of the bridge
CFG_BRIDGE_ADDRESS_METHOD="manual"  # 'auto' or 'manual'


# The IPv4 settings in the block below are only relevant
# when CFG_BRIDGE_ADDRESS_METHOD is 'manual'
CFG_BRIDGE_ADDRESS="192.168.122.84"     # IPv4 address
CFG_BRIDGE_PREFIX_LENGTH="24"           # subnet prefix length
CFG_BRIDGE_GATEWAY="192.168.122.1"      # default gateway
CFG_BRIDGE_DNS_SERVERS="192.168.122.1"  # space separated list of DNS servers
CFG_BRIDGE_DNS_SEARCH="example.com"     # space separated list of domain to search


# Organisation settings
CFG_ORGANISATION="Example Inc."                # The organisation
CFG_ORGANISATION_EMAIL="root"                  # The organisation's contact


# VPN server hostname (must be reachable from the Internet)
CFG_VPN_GATEWAY="192.168.122.84"


# OpenVPN CA server information
export EASYRSA_CA_EXPIRE="3650"                      # Do not adjust this
export EASYRSA_CRL_DAYS="365"                        # Do not adjust this


# E-Mail settings
CFG_EMAIL_SUBJECT="$CFG_ORGANISATION VPN"                       # The subject of the email
CFG_EMAIL_SENDER="$CFG_ORGANISATION <$CFG_ORGANISATION_EMAIL>"  # The sender  of the email
CFG_LANGUAGE_DEFAULT="en_US"


# Windows related information
CFG_VPN_DEVICE_NAME_ON_WINDOWS="OpenVPN1"            # The TUN/TAP interface


# "yes" will add masquerading firewall rules for routed VPN services.
# This is needed when the network router can't properly handle static routes
# back to the VPN server. Setting up static routes is needed when this setting
# is "no" and is not needed when this setting is "yes".
#
# A static route for the settings in this file will route all traffic towards
# 192.168.50.0/255.255.255.128 and 192.168.50.128/255.255.255.128 via the
# VPN server (see the routed OpenVPN services settings below).
CFG_FIREWALL_ADD_MASQ="no"


# The ports that the VPN services listen on
CFG_OPENVPN_PORTS["$CFG_PROTOCOL_UDP-$CFG_METHOD_ROUTED"]=51000  # UDP Routed
CFG_OPENVPN_PORTS["$CFG_PROTOCOL_TCP-$CFG_METHOD_ROUTED"]=51001  # TCP Routed
CFG_OPENVPN_PORTS["$CFG_PROTOCOL_UDP-$CFG_METHOD_BRIDGED"]=51002 # UDP Bridged
CFG_OPENVPN_PORTS["$CFG_PROTOCOL_TCP-$CFG_METHOD_BRIDGED"]=51003 # TCP Bridged


# Routed OpenVPN services
# Make sure the networks do not overlap                     network         netmask
CFG_OPENVPN_SERVER["$CFG_PROTOCOL_UDP-$CFG_METHOD_ROUTED"]="192.168.50.0    255.255.255.128"
CFG_OPENVPN_SERVER["$CFG_PROTOCOL_TCP-$CFG_METHOD_ROUTED"]="192.168.50.128  255.255.255.128"

# Bridged OpenVPN services
# Make sure the pools do not overlap                         gateway         netmask         pool-start-IP   pool-end-IP
CFG_OPENVPN_SERVER["$CFG_PROTOCOL_UDP-$CFG_METHOD_BRIDGED"]="192.168.122.1   255.255.255.0   192.168.122.200 192.168.122.229"
CFG_OPENVPN_SERVER["$CFG_PROTOCOL_TCP-$CFG_METHOD_BRIDGED"]="192.168.122.1   255.255.255.0   192.168.122.230 192.168.122.249"


# Each of these lines will get pushed to clients of routed VPN connections.
# Put the routes of your local LAN(s) in here, DNS information, and any other
# OpenVPN option that you want the clients to know about.
CFG_OPENVPN_PUSHED_ROUTED=(
    "route 192.168.122.0   255.255.255.0"
    "route-gateway         dhcp"
    "dhcp-option DNS       192.168.122.1"
    "dhcp-option DOMAIN    example.com"
    "redirect-gateway      def1"
  )


# Each of these lines will get pushed to clients of bridged VPN connections.
# Put the routes of your local LAN(s) in here, DNS information, and any other
# OpenVPN option that you want the clients to know about.
CFG_OPENVPN_PUSHED_BRIDGED=(
    "${CFG_OPENVPN_PUSHED_ROUTED[@]}"
  )
