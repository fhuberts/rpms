%global configFile  /etc/vpn-server/vpn-server.conf
%global siteName    exampleinc
%global companyName Example Inc.

Name:             vpn-server-site-%{siteName}
Version:          2.3.0
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch


#%package
Summary:          VPN Server Configuration for %{companyName}

Requires:         vpn-server


%description
Configuration for the VPN server of %{companyName}


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -pv "%{buildroot}"
cp -av -t "%{buildroot}" *

mv -v "%{buildroot}%{configFile}" "%{buildroot}/%{configFile}.%{siteName}"


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

if [[ $1 -eq 1 ]]; then
  # install

  if [[ -f "%{configFile}" ]]; then
    rpmSave="%{configFile}.$(date +%%Y%%m%%d-%%H%%M%%S).rpmsave"
    mv -f "%{configFile}" "$rpmSave"
    echo "%{configFile} saved as $rpmSave"
  fi
  cp -f "%{configFile}.%{siteName}" "%{configFile}"
fi

exit 0


%files
%defattr(-,root,root)
%config(noreplace) %{_sysconfdir}/*


%changelog
* Sat Mar 25 2023 Ferry Huberts - 2.3.0
- Adjust to easy-rsa changes

* Tue Apr 20 2021 Ferry Huberts - 2.2.1
- Improve the spec file

* Thu Feb 18 2021 Ferry Huberts - 2.2.0
- Make it possible to have a static IP configuration

* Mon Feb 01 2021 Ferry Huberts - 2.1.0
- Fix the location of the CFG_LANGUAGE_DEFAULT setting
- Add expiration checks and renewal applications
- Talk about certificates, avoid talking about keys

* Wed Jan 27 2021 Ferry Huberts - 2.0.3
- make the default language configurable
- fix a comment

* Sat Jan 16 2021 Ferry Huberts - 2.0.0
- Switch to using easy-rsa 3, as provided by the distro
- Store and use email addresses belonging to keys

* Sat Jan 16 2021 Ferry Huberts - 1.1.5
- Let the VPN be the default gateway
- Add configuration variables for organisation and email
- Replace use of KEY_ORG by CFG_ORGANISATION
- Replace use of KEY_EMAIL by CFG_ORGANISATION_EMAIL
- Update bridge and ethernet interface names
- Do not masquerade by default

* Tue Apr 28 2020 Ferry Huberts - 1.1.4
- Make it build on Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.1.3
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.1.2
- Bump to Fedora 27

* Fri Mar 03 2017 Ferry Huberts - 1.1.1
- Improve %post scriptlet

* Tue Dec 13 2016 Ferry Huberts - 1.1.0
- Use non-git-derived versions

* Mon Dec 12 2016 Ferry Huberts
- Bump to Fedora 25

* Thu Dec 10 2015 Ferry Huberts
- only support Fedora 22 and higher

* Tue Jan 13 2015 Ferry Huberts
- Initial release
