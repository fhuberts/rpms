Name:             base-server
Version:          1.7.1
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms

BuildArch:        noarch




#%package
Summary:          A collection of basic server tools and small utilities

Requires:         sudo, systemd

Recommends:       bash-aliases-dnf, bash-aliases-git-cli, bash-aliases-shell
Recommends:       bash-prompt-git
Recommends:       cockpit-utils, cockpit-ws-enable, configfile-utils
Recommends:       mutt-macros, mutt-maildir
Recommends:       postfix-as-mta
Recommends:       rpm-list-installed
Recommends:       spice-vdagent-enable

Recommends:       avahi, avahi-autoipd, avahi-compat-libdns_sd, avahi-dnsconfd
Recommends:       avahi-tools
Recommends:       bash-completion, bind-utils, btop, bzip2
Recommends:       cockpit, cockpit-bridge, cockpit-dashboard, cockpit-machines
Recommends:       cockpit-networkmanager, cockpit-packagekit, cockpit-podman
Recommends:       cockpit-selinux, cockpit-storaged, cockpit-system
Recommends:       cockpit-utils, cockpit-ws, coreutils
Recommends:       diffutils, dnf-automatic, dnf-utils, dnf-plugin-system-upgrade
Recommends:       gawk, git, git-core, git-core-doc, git-credential-libsecret
Recommends:       git-extras, git-email, git-lfs, git-publish, git-repair, grep
Recommends:       hdparm, htop
Recommends:       iftop, iotop
Recommends:       less, lsof
Recommends:       mailx, man, mc, mutt
Recommends:       net-tools, nfs-utils, nss-mdns, ntpdate, nvme-cli
Recommends:       patch, policycoreutils-python-utils
Recommends:       podman, podman-docker
Recommends:       qemu-guest-agent
Recommends:       rpm, rpmconf
Recommends:       sed, spice-vdagent, symlinks
Recommends:       unzip, util-linux
Recommends:       vim-enhanced
Recommends:       wget
Recommends:       zip

Obsoletes:        base-server-vm <= 1.6.0


%description
A collection of basic server tools and small utilities.

Also enables and disables some services when installed.


%prep
%include ../spec-supported.include


%build


%install


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 1 ]]; then
  # install

  # Disable automatic suspend
  sudo -u gdm dbus-run-session gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 0 &> /dev/null

  # mask undesired targets
  for target in \
      "sleep.target" "suspend.target" "hibernate.target" "hybrid-sleep.target"; do
    systemctl -q mask    "$target"
  done

  # disable undesired services
  for service in \
      "abrtd.service" "packagekit.service"; do
    systemctl -q stop    "$service"
    systemctl -q disable "$service"
  done

  # enable and restart desired services
  for service in \
      "avahi-dnsconfd.service" \
      "dnf-automatic-download.timer"; do
    systemctl -q enable            "$service"
    systemctl -q reload-or-restart "$service"
  done
fi

exit 0


%files
%defattr(-,root,root)


%changelog
* Thu Mar 06 2025 Ferry Huberts - 1.7.1
- Add htop and btop

* Fri May 19 2023 Ferry Huberts - 1.7.0
- Disable automatic suspend

* Fri Apr 28 2023 Ferry Huberts - 1.6.34
- Add podman and podman-docker

* Sat Feb 05 2022 Ferry Huberts - 1.6.33
- Update package dependencies

* Sat Feb 05 2022 Ferry Huberts - 1.6.32
- Disable packagekit service

* Sun Jan 16 2022 Ferry Huberts - 1.6.31
- Fix small mistake in previous update

* Fri Dec 31 2021 Ferry Huberts - 1.6.30
- Tweak service enable/disable on install

* Tue Apr 27 2021 Ferry Huberts - 1.6.29
- Add some dependencies for dnf system upgrade

* Sat Apr 17 2021 Ferry Huberts - 1.6.28
- Add git dependencies
- Tweak starting services

* Sat Apr 17 2021 Ferry Huberts - 1.6.27
- Remove disabling obsolete services
- Update dependencies

* Wed Mar 03 2021 Ferry Huberts - 1.6.26
- Add dnf-automatic

* Sun Jul 05 2020 Ferry Huberts - 1.6.25
- Bump to Fedora 32

* Fri Jan 10 2020 Ferry Huberts - 1.6.24
- Add nvme-cli

* Wed Oct 30 2019 Ferry Huberts - 1.6.23
- Add dnf-utils

* Wed Aug 28 2019 Ferry Huberts - 1.6.22
- Add iftop

* Thu Aug 22 2019 Ferry Huberts - 1.6.21
- Remove cockpit-machines

* Wed Aug 21 2019 Ferry Huberts - 1.6.20
- Add iotop

* Sun Aug 11 2019 Ferry Huberts - 1.6.19
- Disable libvirtd.service and vmtoolsd.service too

* Wed Jun 06 2018 Ferry Huberts - 1.6.18
- The repository moved to GitLab

* Sun Dec 31 2017 Ferry Huberts - 1.6.17
- Bump to Fedora 27
- Do not recommend tmp-on-disc
- Install some extra cockpit plugins

* Thu Jun 15 2017 Ferry Huberts - 1.6.16
- Do not recommend grub-timeout-zero

* Wed Mar 01 2017 Ferry Huberts - 1.6.15
- Improve %post scriptlet

* Mon Dec 12 2016 Ferry Huberts - 1.6.14
- Add mutt-macros and mutt-maildir as dependencies

* Fri Dec 02 2016 Ferry Huberts - 1.6.13
- Bump to Fedora 25
- Remove avahi-daemon-enable-ipv6, already enabled by default on Fedora 25
- Remove chrony-enable, already enabled by default on Fedora 25

* Tue May 10 2016 Ferry Huberts - 1.6.12
- Add postfix-as-mta as a dependency

* Thu Apr 28 2016 Ferry Huberts - 1.6.11
- Split off chrony network time enablement into chrony-enable

* Thu Apr 28 2016 Ferry Huberts - 1.6.10
- Split off fstrim timer enablement into fstrim-enable

* Thu Apr 28 2016 Ferry Huberts - 1.6.9
- Split off spice agent enablement into spice-vdagent-enable

* Thu Apr 28 2016 Ferry Huberts - 1.6.8
- Split off setting grub timeout to zero into grub-timeout-zero

* Thu Apr 28 2016 Ferry Huberts - 1.6.7
- Split off putting /tmp on disc into tmp-on-disc

* Tue Apr 26 2016 Ferry Huberts - 1.6.6
- Split off cockpit enablement into cockpit-ws-enable

* Tue Apr 26 2016 Ferry Huberts - 1.6.5
- Split off avahi IPv6 enablement into avahi-daemon-enable-ipv6

* Sun Apr 24 2016 Ferry Huberts - 1.6.4
- require all packages we're changing things in and with

* Wed Dec 23 2015 Ferry Huberts - 1.6.3
- fix a comment

* Mon Dec 21 2015 Ferry Huberts - 1.6.2
- enable and start postfix too

* Mon Dec 21 2015 Ferry Huberts - 1.6.1
- do not show any output for systemctl

* Mon Dec 21 2015 Ferry Huberts - 1.6.0
- fold base-server-vm package into base-server package

* Mon Dec 21 2015 Ferry Huberts - 1.5.0
- use configfile-adjust-settings to enable IPv6 avahi
- minor improvement to tmp.mount masking
- use cockpit-get-port to open the actual cockpit port
- update the list of abrt and avahi services to modify
- only modify services when needed

* Fri Dec 18 2015 Ferry Huberts - 1.4.4
- add lsof to the dependencies
- disable /tmp on tmpfs when enabled and no /tmp in /etc/fstab

* Thu Dec 17 2015 Ferry Huberts - 1.4.3
- also enable fstrim and chronyd
- no need to enable and restart cockpit.service

* Thu Dec 17 2015 Ferry Huberts - 1.4.2
- ensure cockpit is started

* Tue Dec 15 2015 Ferry Huberts - 1.4.1
- add configfile-utils as a dependency

* Mon Dec 14 2015 Ferry Huberts - 1.4.0
- recommend packages instead of requiring them; this is a meta package

* Mon Dec 14 2015 Ferry Huberts - 1.3.0
- move listing installed packages into its own package

* Mon Dec 14 2015 Ferry Huberts - 1.2.1
- add forgotten dependency on rpm

* Mon Dec 14 2015 Ferry Huberts - 1.2.0
- move cockpit utilities into their own package

* Sat Dec 12 2015 Ferry Huberts - 1.1.2
- improve the cockpit-port-move script

* Fri Dec 11 2015 Ferry Huberts - 1.1.1
- use quiet mode for firewall-cmd
- only support Fedora 23 and higher (further cleanup)

* Wed Dec 09 2015 Ferry Huberts - 1.1.0
- refresh and clean up 'Requires'
- only support Fedora 23 and higher

* Wed Dec 09 2015 Ferry Huberts - 1.0.4
- depend on bash-aliases-dnf as well for Fedora 23
- run systemctl in quiet mode

* Fri Nov 13 2015 Ferry Huberts - 1.0.3
- Add chrony to Fedora 23

* Fri Nov 13 2015 Ferry Huberts - 1.0.2
- Add cockpit to Fedora 23

* Thu Nov 12 2015 Ferry Huberts - 1.0.1
- Update for Fedora 23

* Thu Jun 25 2015 Ferry Huberts - 1.0.0-2
- Support Fedora >= 21

* Sun Jan 25 2015 Ferry Huberts - 1.0.0
- Initial release
