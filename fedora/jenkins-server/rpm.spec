%global jenkinsDir        /var/lib/jenkins
%global jenkinsAuditDir   %{jenkinsDir}/audit

Name:             jenkins-server
Version:          1.1.4
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Basic setup of a Jenkins server, with some extra utilities

Requires:         bash, bash-completion-pelagic-helpers
Requires:         configfile-utils, coreutils
Requires:         findutils, firewalld
Requires:         hostname
Requires:         java-21-openjdk, jenkins
Requires:         sed
Requires:         tar
Requires:         systemd


%description
Basic setup of a Jenkins server, with some extra utilities.

This packages assumes that - before installing this package - a dnf repository
is configured that points to the Jenkins upstream repository by running the
following commands as root:
  dnf install -y wget rpm
  wget -O "/etc/yum.repos.d/jenkins.repo" \
          "http://pkg.jenkins-ci.org/redhat/jenkins.repo"
  rpm --import "http://pkg.jenkins-ci.org/redhat/jenkins-ci.org.key"


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


# Scriptlets have one argument and MUST exit with status 0.
# +------------+---------+---------+-----------+
# |            | install | upgrade | uninstall |
# +------------+---------+---------+-----------+
# | %pretrans  | $1 == 0 | $1 == 0 |   (N/A)   |
# | %pre       | $1 == 1 | $1 == 2 |   (N/A)   |
# | %post      | $1 == 1 | $1 == 2 |   (N/A)   |
# | %preun     |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %postun    |  (N/A)  | $1 == 1 |  $1 == 0  |
# | %posttrans | $1 == 0 | $1 == 0 |   (N/A)   |
# +------------+---------+---------+-----------+
#
# The scriptlets in %pre and %post are respectively run before and after a
# package is installed. The scriptlets %preun and %postun are run before and
# after a package is uninstalled. The scriptlets %pretrans and %posttrans
# are run at start and end of a transaction. On upgrade, the scripts are run
# in the following order:
#  1. %pretrans      of new package
#  2. %pre           of new package
#  3.                (package install)
#  4. %post          of new package
#  5. %triggerin     of other packages (set off by installing new package)
#  6. %triggerin     of new package (if any are true)
#  7. %triggerun     of old package (if it's set off by uninstalling the old
#                    package)
#  8. %triggerun     of other packages (set off by uninstalling old package)
#  9. %preun         of old package
# 10.                (removal of old package)
# 11. %postun        of old package
# 12. %triggerpostun of old package (if it's set off by uninstalling the old
#                    package)
# 13. %triggerpostun of other packages (if they're setu off by uninstalling
#                    the old package)
# 14. %posttrans     of new package

%post

# install and updates

if [[ $1 -eq 2 ]]; then
  # updates

  systemctl -q try-reload-or-restart "jenkins.service"
fi

if [[ $1 -eq 1 ]]; then
  # install

  jenkinsPort=$(jenkins-get-port)
  for i in "" "--permanent"; do
    firewall-cmd -q $i --add-port=$jenkinsPort/tcp
  done

  systemctl -q enable            "jenkins.service"
  systemctl -q reload-or-restart "jenkins.service"
fi

exit 0


%preun

if [[ $1 -eq 0 ]]; then
  # uninstall

  jenkinsPort=$(jenkins-get-port)
  for i in "" "--permanent"; do
    firewall-cmd -q $i --remove-port=$jenkinsPort/tcp
  done

  systemctl -q stop    "jenkins.service"
  systemctl -q disable "jenkins.service"
fi

exit 0


%files
%defattr(-,root,root)
%{_bindir}/*
%{_sbindir}/*
%{_datarootdir}/bash-completion/completions/*
%{_datarootdir}/%{name}/*
%dir %attr(-,jenkins,jenkins) %{jenkinsAuditDir}


%changelog
* Thu Mar 06 2025 Ferry Huberts - 1.1.4
- Fix getting the Jenkins port

* Wed Mar 05 2025 Ferry Huberts - 1.1.3
- Update openjdk version

* Sun Apr 18 2021 Ferry Huberts - 1.1.2
- Improve the scripts
- Improve the spec file
- Tweak starting services

* Wed Feb 24 2021 Ferry Huberts - 1.1.1
- Remove the Coverity fix

* Wed Feb 24 2021 Ferry Huberts - 1.1.0
- Improve usage and add bash completion

* Sun Jul 05 2020 Ferry Huberts - 1.0.13
- Bump to Fedora 32

* Wed Jun 06 2018 Ferry Huberts - 1.0.12
- The repository moved to GitLab

* Fri Jan 19 2018 Ferry Huberts - 1.0.11
- Improve coverity header to fix the _Float128 issue

* Fri Jan 19 2018 Ferry Huberts - 1.0.10
- Add coverity header to fix the _Float128 issue

* Sun Dec 31 2017 Ferry Huberts - 1.0.9
- Bump to Fedora 27
- Silence systemctl calls
- Fix some typos

* Mon Apr 17 2017 Ferry Huberts - 1.0.8
- Use bash' [[ builtin instead of the program [

* Thu Mar 02 2017 Ferry Huberts - 1.0.7
- Improve %post scriptlet
- Remove %preun scriptlet

* Wed Dec 28 2016 Ferry Huberts - 1.0.6
- systemctl changes

* Tue Dec 06 2016 Ferry Huberts - 1.0.5
- Bump to Fedora 25

* Mon Apr 25 2016 Ferry Huberts - 1.0.4
- Fix checking the exit code of systemctl

* Wed Dec 30 2015 Ferry Huberts - 1.0.3
- Fix a typo in jenkins-move-port

* Wed Dec 30 2015 Ferry Huberts - 1.0.2
- Improve jenkins-backup

* Wed Dec 30 2015 Ferry Huberts - 1.0.1
- Fix execute bit on jenkins-backup

* Tue Dec 29 2015 Ferry Huberts - 1.0.0
- Initial release
