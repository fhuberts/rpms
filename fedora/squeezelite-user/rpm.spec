Name:             squeezelite-user
Version:          1.0.1
Release:          1%{?dist}

License:          GPLv2+
URL:              https://gitlab.com/fhuberts/rpms
Source0:          %{name}-%{version}.tar.gz

BuildArch:        noarch




#%package
Summary:          Squeezelite user session support

Requires:         bash
Requires:         coreutils
Requires:         libnotify
Requires:         squeezelite
Requires:         systemd


%description
Squeezelite user session support.


%prep
%include ../spec-supported.include
%setup -q -n %{name}-%{version}


%build


%install
mkdir -p "%{buildroot}"
cp -a -t "%{buildroot}" *


%files
%defattr(-,root,root)
%{_bindir}/*
%{_datarootdir}/applications/*
%{_datarootdir}/%{name}/*


%changelog
* Fri Aug 19 2022 Ferry Huberts - 1.0.1
- Add the player name to the notification

* Fri Aug 19 2022 Ferry Huberts - 1.0.0
- Initial release
